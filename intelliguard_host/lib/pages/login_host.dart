import 'package:flutter/material.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:intelliguard_host/provider_model.dart/host_worker_model.dart';
import 'package:intelliguard_host/services/auth.dart';
import 'package:intelliguard_host/services/locator.dart';
import 'package:intelliguard_host/ui/form.dart';
import 'package:intelliguard_host/ui/title.dart';
import 'package:provider/provider.dart';
import 'package:intelliguard_host/ui/alert_error.dart';

class LoginHost extends StatefulWidget {
  final String org;
  LoginHost(this.org);
  @override
  LoginHostState createState() => LoginHostState();
}

class LoginHostState extends State<LoginHost> {
  final _formKey = GlobalKey<FormState>();
  final services = locator<Auth>();
  void _getData(String input) {
    String page;
    final worker = Provider.of<HostWorkerModel>(context, listen: false);
    final events = Provider.of<EventsModel>(context, listen: false);
    worker.add(input).then((isHead) {
      services.login(widget.org, input);
      if (isHead)
        page = 'host';
      else
        page = 'worker';
      events.checkForPassedEvents().then((_) {
        Navigator.pop(context);
        Navigator.pushReplacementNamed(context, page);
      });
    }, onError: (error) {
      alertError(context, error);
    });
  }

  void _handlePress() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.only(top: height * 0.05),
      margin: EdgeInsets.symmetric(horizontal: width * 0.075),
      height: height * 0.4 + MediaQuery.of(context).viewInsets.bottom,
      child: Column(
        children: <Widget>[
          FormTitle('Enter ID'),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: height * 0.02,
                ),
                FormInput(
                  svg: './assets/images/icon_profile.svg',
                  placeholder: 'ID',
                  getData: _getData,
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                Button(
                  text: 'LOGIN',
                  handlePress: _handlePress,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
