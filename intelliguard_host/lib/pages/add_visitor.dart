import 'package:flutter/material.dart';
import 'package:intelliguard_host/model/visitor.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:intelliguard_host/ui/form.dart';
import 'package:intelliguard_host/ui/title.dart';
import 'package:provider/provider.dart';
import 'package:intelliguard_host/ui/alert_error.dart';

class AddVisitor extends StatefulWidget {
  final String eventID;

  AddVisitor(this.eventID);
  @override
  _AddVisitorState createState() => _AddVisitorState();
}

class _AddVisitorState extends State<AddVisitor> {
  final _formKey = GlobalKey<FormState>();
  String ic;
  String name;
  String email;
  String age;
  void _getIC(String input) => ic = input;

  void _getName(String input) => name = input;

  void _getEmail(String input) => email = input;

  void _getAge(String input) => age = input;

  void _submitForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      final events = Provider.of<EventsModel>(context, listen: false);
      final newVisitor = Visitor(name, ic, age, email, false);
      events.addVisitor(widget.eventID, newVisitor).then((_) {
        if (!_) alertError(context, 'Visitor already exist.');
        Navigator.pop(context);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    return Container(
      height: height * 0.8 + MediaQuery.of(context).viewInsets.bottom,
      padding: EdgeInsets.only(
          top: height * 0.1, bottom: MediaQuery.of(context).viewInsets.bottom),
      margin: EdgeInsets.symmetric(horizontal: width * 0.075),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              FormTitle('New Visitor'),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                svg: 'assets/images/icon_ic.svg',
                placeholder: 'IC',
                getData: _getIC,
              ),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                svg: 'assets/images/icon_profile.svg',
                placeholder: 'Name',
                getData: _getName,
              ),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                svg: 'assets/images/icon_email.svg',
                placeholder: 'Email',
                getData: _getEmail,
              ),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                svg: 'assets/images/icon_age.svg',
                placeholder: 'Age',
                getData: _getAge,
              ),
              SizedBox(
                height: height * 0.06,
              ),
              Button(
                text: 'SUBMIT',
                handlePress: _submitForm,
              )
            ],
          ),
        ),
      ),
    );
  }
}
