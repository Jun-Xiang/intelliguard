import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_host/model/ui_color.dart';
import 'package:intelliguard_host/pages/login_host.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:intelliguard_host/provider_model.dart/host_worker_model.dart';
import 'package:intelliguard_host/services/auth.dart';
import 'package:intelliguard_host/services/locator.dart';
import 'package:intelliguard_host/ui/form.dart';
import 'package:intelliguard_host/ui/alert_error.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formkey = GlobalKey<FormState>();
  final UiColor color = UiColor();
  Auth services = locator<Auth>();

  String _orgName;
  String _pw;
  bool obscure = true;
  _showHidePw() {
    setState(() {
      obscure = !obscure;
    });
  }

  void _getOrgName(input) => _orgName = input;
  void _getPw(input) => _pw = input;
  bool _keyboardIsVisible() {
    return !(MediaQuery.of(context).viewInsets.bottom == 0.0);
  }

  void _handleLoginPress() async {
    final form = _formkey.currentState;
    if (form.validate()) {
      form.save();
      FocusScope.of(context).requestFocus(FocusNode());
      bool exists = await services.orgExists(_orgName);
      if (exists) {
        String pw = await services.orgPassword(_orgName);
        if (pw == _pw) {
          services.registerOrg(_orgName);
          showModalBottomSheet(
              isScrollControlled: true,
              backgroundColor: color.grey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(67),
                  topRight: Radius.circular(67),
                ),
              ),
              context: context,
              builder: (_) => LoginHost(_orgName));
        } else {
          alertError(context, 'Incorrect Password');
        }
      } else {
        alertError(context, 'Organisation doesn\'t exist!');
      }
    }
  }

  void _handleSignUpPress() {
    Navigator.pushNamed(context, 'signup');
  }

  @override
  void initState() {
    services.isLoggedIn().then((isLoggedIn) {
      if (isLoggedIn) {
        String page;
        final worker = Provider.of<HostWorkerModel>(context, listen: false);
        final events = Provider.of<EventsModel>(context, listen: false);
        services.getID().then((id) {
          worker.add(id).then((isHead) {
            if (isHead)
              page = 'host';
            else
              page = 'worker';
            events.checkForPassedEvents().then((_) {
              Navigator.pushReplacementNamed(context, page);
            });
          }, onError: (error) {
            alertError(context, error);
          });
        });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color.fromRGBO(14, 24, 38, 1),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: width * 0.075),
          alignment: Alignment.center,
          child: SingleChildScrollView(
            // shrinkWrap: true,
            physics: _keyboardIsVisible()
                ? BouncingScrollPhysics()
                : NeverScrollableScrollPhysics(),
            child: Column(
              children: <Widget>[
                SvgPicture.asset('assets/images/illustration_login.svg'),
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Text(
                    'INTELLIGUARD',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 34,
                      fontFamily: 'Pier',
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 30),
                  child: Text(
                    'Host',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 21,
                      fontFamily: 'Pier',
                      fontWeight: FontWeight.w300,
                      color: Colors.white,
                    ),
                  ),
                ),
                Form(
                  key: _formkey,
                  child: Column(
                    children: <Widget>[
                      FormInput(
                        svg: 'assets/images/icon_profile.svg',
                        placeholder: 'Organisation Name',
                        getData: _getOrgName,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      FormInput(
                        svg: 'assets/images/icon_lock.svg',
                        placeholder: 'Password',
                        obscure: obscure,
                        showHidePw: _showHidePw,
                        getData: _getPw,
                      ),
                      SizedBox(
                        height: height * 0.08,
                      ),
                      Button(
                        text: 'LOGIN',
                        handlePress: _handleLoginPress,
                        textColor: Theme.of(context).primaryColor,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ButtonOutline(
                        text: 'SIGNUP',
                        handlePress: _handleSignUpPress,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
