import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intelliguard_host/model/org_event.dart';
import 'package:intelliguard_host/pages/host/view_event.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:provider/provider.dart';

import 'show_form.dart';
import '../../ui/list_shape.dart';
import '../../ui/list_button.dart';
import '../../ui/title.dart';
import '../../model/ui_color.dart';

class EventsPage extends StatelessWidget {
  final UiColor color = UiColor();

  Widget _buildContent(BuildContext context, OrgEvent event) {
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 8,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTitle(event.eventName, hasPassed: event.hasPassed),
                Description(event.host),
              ],
            ),
          ),
          !event.hasPassed
              ? ListButton(
                  svg: './assets/images/icon_add.svg',
                  handlePress: () {
                    showForm(context, 'visitor');
                  },
                )
              : Container(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final events = Provider.of<EventsModel>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        PageTitle('Events'),
        SizedBox(
          height: 22,
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
              stream: events.fetchEvents(),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  final docs = snapshot.data.documents;
                  List<OrgEvent> events = docs.map((event) {
                    final data = event.data;
                    return OrgEvent(
                        event.documentID,
                        data['name'],
                        data['time'],
                        data['date'],
                        data['venue'],
                        data['host'],
                        data['hasPassed']);
                  }).toList();
                  return ListView.builder(
                    itemCount: docs.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListShape(
                        _buildContent(context, events[index]),
                        () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ViewEvent(events[index]),
                          ),
                        ),
                      );
                    },
                  );
                } else {
                  return Center(
                      child: SizedBox(
                          height: 100,
                          width: 100,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          )));
                }
              }),
        ),
      ],
    );
  }
}
