import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_host/model/ui_color.dart';
import 'package:intelliguard_host/pages/host/events_page.dart';
import 'package:intelliguard_host/pages/host/settings.dart';
import 'package:intelliguard_host/pages/host/show_form.dart';

class Host extends StatefulWidget {
  @override
  _HostState createState() => _HostState();
}

class _HostState extends State<Host> {
  final UiColor color = UiColor();
  Map<int, bool> selected = {
    1: true,
    2: false,
  };
  Map<int, String> nav = {
    1: './assets/images/icon_event.svg',
    2: './assets/images/icon_settings.svg',
  };

  void handlePress(int pos) {
    setState(() {
      selected.updateAll((i, b) => selected[i] = false);
      selected[pos] = true;
    });
  }

  List<Widget> _buildNav(double height, double width, Function handlePress) {
    List<Widget> navItems = [];
    nav.forEach((int i, String s) {
      navItems.add(SizedBox(
        width: width * 0.15,
        height: width * 0.15,
        child: FlatButton(
          padding: EdgeInsets.all(0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(13)),
          onPressed: () => handlePress(i),
          splashColor: color.black,
          focusColor: color.black,
          color: selected[i] ? color.black : null,
          highlightColor: color.black,
          child: SvgPicture.asset(
            s,
            width: width * 0.08,
          ),
        ),
      ));
    });
    return navItems.toList();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    Widget content;
    int selectedNum;

    selected.forEach(
        (int i, bool b) => selected[i] == true ? selectedNum = i : null);

    switch (selectedNum) {
      case 1:
        content = EventsPage();
        break;
      case 2:
        content = SettingsPage();
        break;
    }

    return Scaffold(
      backgroundColor: color.black,
      floatingActionButton: FloatingActionButton(
        onPressed: () {showForm(context, 'event');},
        child: SvgPicture.asset('assets/images/icon_add.svg'),
        backgroundColor: color.button,
      ),
      bottomNavigationBar: BottomAppBar(
        color: color.grey,
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: height * 0.1,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: _buildNav(height, width, handlePress),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(top: height * 0.05),
          margin: EdgeInsets.symmetric(horizontal: width * 0.075),
          child: content,
        ),
      ),
    );
  }
}
