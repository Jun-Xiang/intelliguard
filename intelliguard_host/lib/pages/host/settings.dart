import 'package:flutter/material.dart';
import 'package:intelliguard_host/provider_model.dart/host_worker_model.dart';
import 'package:intelliguard_host/services/api.dart';
import 'package:intelliguard_host/services/auth.dart';
import 'package:intelliguard_host/services/locator.dart';
import 'package:intelliguard_host/ui/alert_text_style.dart';
import 'package:intelliguard_host/ui/form.dart';
import 'package:intelliguard_host/ui/list_shape.dart';
import 'package:intelliguard_host/ui/title.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  Api _api = locator<Api>();
  void _logOut() {
    final worker = Provider.of<HostWorkerModel>(context, listen: false);
    final services = locator<Auth>();
    worker.clearWorker();
    services.logout().then((_) {
      Navigator.pop(context);
      Navigator.pushReplacementNamed(context, '/');
    });
  }

  void _handlePress() {
    showDialog(
      context: context,
      child: AlertDialog(
        actions: <Widget>[
          FlatButton(
            child: Text(
              'Cancel',
              style: buttonStyle,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          FlatButton(
              child: Text('Log Out', style: buttonStyle), onPressed: _logOut)
        ],
        title: Text('Confirm Log Out?', style: titleStyle),
        content: Text(
          'This action cannot be undone.',
          style: contentStyle,
        ),
      ),
    );
  }

  Widget _buildContent(String action, String role) {
    return ListTile(
      title: Text('$action $role ', style: TextStyle(fontFamily: 'Pier')),
      subtitle: Text('Tap to edit',
          style: TextStyle(
            fontFamily: 'Pier',
          )),
      trailing: Icon(Icons.edit),
    );
  }

  _updateFailedDialog(msg) {
    showDialog(
        context: context,
        child: AlertDialog(
          title: Text(
            msg,
            style: titleStyle,
          ),
          content: Text(
            'Please make sure you entered the correct details!',
            style: contentStyle,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () => Navigator.pop(context),
            )
          ],
        ));
  }

  _afterAction(success, msg, err) {
    if (success) {
      showDialog(
          context: context,
          child: AlertDialog(
            title: Text(
              msg,
              style: titleStyle,
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  }),
            ],
          ));
    } else {
      _updateFailedDialog(err);
    }
  }

  _updateHead(old, newI) {
    _api.updateHeadName(old, newI).then((success) {
      if (success) {
        showDialog(
            context: context,
            child: AlertDialog(
              title: Text(
                'Update Completed',
                style: titleStyle,
              ),
              content: Text(
                'We will log you out after this!',
                style: contentStyle,
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: _logOut,
                )
              ],
            ));
      } else {
        _updateFailedDialog('Update Failed!');
      }
    });
  }

  _updateOrgPw(old, newI) {
    _api.updatePw('password', old, newI).then((success) {
      if (success) {
        showDialog(
            context: context,
            child: AlertDialog(
              title: Text(
                'Update Completed',
                style: titleStyle,
              ),
              content: Text(
                'We will log you out after this!',
                style: contentStyle,
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: _logOut,
                )
              ],
            ));
      } else {
        _updateFailedDialog('Update Failed!');
      }
    });
  }

  _updateGuardPw(old, newI) {
    _api.updatePw('guard_password', old, newI).then((success) {
      _afterAction(success, 'Update Completed!', 'Update Failed!');
    });
  }

  _addGuardID(id) {
    _api.addID('guard', id, false).then((_) {
      _afterAction(true, 'Add Completed!', 'Add Failed!');
    }).catchError((err) {
      _afterAction(false, 'Add Completed!', 'An error has occured!');
    });
  }

  _addWorkerID(id) {
    _api.addID('worker', id, false).then((_) {
      _afterAction(true, 'Add Completed!', 'Add Failed!');
    }).catchError((err) {
      _afterAction(false, 'Add Completed!', 'An error has occured!');
    });
  }

  _deleteWorker(id) {
    _api.delete('worker/$id').then((_) {
      _afterAction(true, 'Worker deleted!', 'Delete Failed!');
    }).catchError((err) {
      _afterAction(false, 'Worker deleted!', 'ID not found');
    });
  }

  _deleteGuard(id) {
    _api.delete('guard/$id').then((_) {
      _afterAction(true, 'Guard deleted!', 'Delete Failed!');
    }).catchError((err) {
      _afterAction(false, 'Worker deleted!', 'ID not found');
    });
  }

  _showEditDialog(action, role) {
    String old;
    void getOld(input) => old = input;
    String newI;
    void getNew(input) => newI = input;
    Widget content;
    switch (action) {
      case 'Edit':
        content = _editForm(role, getNew, getOld);
        break;
      case 'Add':
        content = _addForm(role, getNew);
        break;
      case 'Delete':
        content = _deleteForm(role, getNew);
        break;
      default:
    }
    final _formKey = GlobalKey<FormState>();
    handlePress() {
      final form = _formKey.currentState;
      if (form.validate()) {
        form.save();
        if (action == 'Edit') {
          switch (role) {
            case 'Organisation Head name':
              _updateHead(old, newI);
              break;
            case 'Organisation password':
              _updateOrgPw(old, newI);
              break;
            case 'Guard password':
              _updateGuardPw(old, newI);
              break;
          }
        } else if (action == 'Add') {
          switch (role) {
            case 'Guard ID':
              _addGuardID(newI);
              break;
            case 'Worker ID':
              _addWorkerID(newI);
              break;
          }
        } else {
          switch (role) {
            case 'Guard ID':
              _deleteGuard(newI);
              break;
            case 'Worker ID':
              _deleteWorker(newI);
              break;
          }
        }
      }
    }

    showDialog(
        context: context,
        child: AlertDialog(
          title: Text('$action $role'),
          content: Form(key: _formKey, child: content),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: handlePress,
            ),
            FlatButton(
                child: Text('Cancel'), onPressed: () => Navigator.pop(context))
          ],
        ));
  }

  _editForm(role, getNew, getOld) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        FormInput(
          placeholder: 'Old $role',
          getData: getOld,
        ),
        FormInput(
          placeholder: 'New $role',
          getData: getNew,
        )
      ],
    );
  }

  _addForm(role, getNew) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        FormInput(
          placeholder: 'New $role',
          getData: getNew,
        )
      ],
    );
  }

  _deleteForm(role, getNew) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        FormInput(
          placeholder: '$role',
          getData: getNew,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        PageTitle('Settings'),
        ListShape(_buildContent('Edit', 'Organisation Head name'),
            () => _showEditDialog('Edit', 'Organisation Head name')),
        ListShape(_buildContent('Add', 'Guard ID'),
            () => _showEditDialog('Add', 'Guard ID')),
        ListShape(_buildContent('Delete', 'Guard ID'),
            () => _showEditDialog('Delete', 'Guard ID')),
        ListShape(_buildContent('Add', 'Worker ID'),
            () => _showEditDialog('Add', 'Worker ID')),
        ListShape(_buildContent('Delete', 'Worker ID'),
            () => _showEditDialog('Delete', 'Worker ID')),
        ListShape(_buildContent('Edit', 'Organisation password'),
            () => _showEditDialog('Edit', 'Organisation password')),
        ListShape(_buildContent('Edit', 'Guard password'),
            () => _showEditDialog('Edit', 'Guard password')),
        SizedBox(
          height: 20,
        ),
        Button(
          text: 'LOGOUT',
          handlePress: () => _handlePress(),
        ),
        SizedBox(
          height: 40,
        ),
      ],
    );
  }
}
