import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_host/model/ui_color.dart';
import 'package:intelliguard_host/model/visitor.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:intelliguard_host/ui/label.dart';
import 'package:intelliguard_host/ui/list_button.dart';
import 'package:intelliguard_host/ui/list_shape.dart';
import 'package:intelliguard_host/ui/title.dart';
import 'package:provider/provider.dart';

class VisitorsPage extends StatelessWidget {
  final String eventID;

  VisitorsPage(this.eventID);

  final UiColor color = UiColor();

  void _handleTap() {}

  Widget _buildContent(BuildContext context, Visitor visitor) {
    final events = Provider.of<EventsModel>(context);
    return Padding(
      padding: EdgeInsets.only(left: 20.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 8,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ListTitle(visitor.name),
                    Label(
                      label: visitor.present ? 'PRESENT' : 'ABSENT',
                      color: visitor.present ? color.green : color.red,
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Description(visitor.ic),
                Description('${visitor.age} years old')
              ],
            ),
          ),
          ListButton(
            svg: 'assets/images/icon_dash.svg',
            handlePress: () => events.deleteVisitor(eventID, visitor.ic),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    final events = Provider.of<EventsModel>(context);

    return Scaffold(
      backgroundColor: color.black,
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: width * 0.075),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: height * 0.05,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () => Navigator.pop(context),
                  icon: SvgPicture.asset('./assets/images/icon_arrow_left.svg'),
                ),
              ),
              SizedBox(
                height: height * 0.04,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: <Widget>[
                    Expanded(child: PageTitle('Visitors')),
                    StreamBuilder<QuerySnapshot>(
                        stream: events.loadVisitors(eventID),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            int present = 0;
                            int absent = 0;
                            final data = snapshot.data;
                            final docs = data.documents;
                            docs.forEach((v) {
                              final data = v.data;
                              data['present'] ? present++ : absent++;
                            });
                            return Column(
                              children: <Widget>[
                                Text(
                                  'Present: $present',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Pier',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 15,
                                  ),
                                ),
                                Text(
                                  'Absent: $absent',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Pier',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 15,
                                  ),
                                ),
                              ],
                            );
                          } else {
                            return CircularProgressIndicator(backgroundColor: Colors.white,);
                          }
                        })
                  ],
                ),
              ),
              SizedBox(
                height: 22,
              ),
              Flexible(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: StreamBuilder<QuerySnapshot>(
                      stream: events.loadVisitors(eventID),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          final data = snapshot.data;
                          final docs = data.documents;
                          List<Visitor> visitors = docs.map((v) {
                            final data = v.data;
                            return Visitor(data['name'], v.documentID,
                                data['age'], data['email'], data['present']);
                          }).toList();
                          return ListView.builder(
                              itemCount: visitors.length,
                              itemBuilder: (BuildContext context, int index) {
                                return ListShape(
                                  _buildContent(context, visitors[index]),
                                  _handleTap,
                                );
                              });
                        } else {
                          return Center(
                              child: SizedBox(
                                  height: 100,
                                  width: 100,
                                  child: CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  )));
                        }
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
