import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_host/model/org_event.dart';
import 'package:intelliguard_host/model/ui_color.dart';
import 'package:intelliguard_host/pages/host/visitors_page.dart';
import 'package:intelliguard_host/pages/host/show_form.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:intelliguard_host/ui/alert_text_style.dart';
import 'package:intelliguard_host/ui/form.dart';
import 'package:intelliguard_host/ui/title.dart';
import 'package:provider/provider.dart';

class ViewEvent extends StatelessWidget {
  final OrgEvent event;
  final UiColor color = UiColor();

  ViewEvent(this.event);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: color.black,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: width * 0.075),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: height * 0.05,
                ),
                IconButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () => Navigator.pop(context),
                  icon: SvgPicture.asset('./assets/images/icon_arrow_left.svg'),
                ),
                SizedBox(
                  height: height * 0.04,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      EventTitle(event.eventName),
                      SizedBox(
                        height: height * 0.04,
                      ),
                      EventDetails('Date: ', event.date),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      EventDetails('Time: ', event.time),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      EventDetails('Venue: ', event.venue),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      EventDetails('Host: ', event.host),
                      SizedBox(
                        height: height * 0.06,
                      ),
                      !event.hasPassed ? 
                      Button(
                        text: 'Add Visitor',
                        handlePress: () {
                          showForm(context, 'visitor', eventID: event.id);
                        },
                      ) : Container(), 
                      SizedBox(
                        height: height * 0.02,
                      ),
                      Button(
                        text: 'View Insights',
                        handlePress: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    VisitorsPage(event.id)),
                          );
                        },
                      ),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      Button(
                        text: 'Delete Event',
                        handlePress: () {
                          showDialog(
                              context: context,
                              child: AlertDialog(
                                title: Text(
                                    'Are you sure you want to delete this event?',
                                    style: titleStyle),
                                content: Text(
                                  'This action cannot be undone.',
                                  style: contentStyle,
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      'Cancel',
                                      style: buttonStyle,
                                    ),
                                    onPressed: () => Navigator.pop(context),
                                  ),
                                  FlatButton(
                                      child: Text('Delete', style: buttonStyle),
                                      onPressed: () {
                                        final events =
                                            Provider.of<EventsModel>(context);
                                        events.deleteEvent(event.id);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      })
                                ],
                              ));
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
