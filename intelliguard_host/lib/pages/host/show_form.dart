import 'package:flutter/material.dart';
import 'package:intelliguard_host/pages/add_visitor.dart';
import 'package:intelliguard_host/pages/host/add_event.dart';
import 'package:intelliguard_host/model/ui_color.dart';


showForm(BuildContext context, action, {eventID}) {
  final UiColor color = UiColor();
  Widget content;
  switch(action) {
    case 'event':
      content = AddEvent();
      break;
    case 'visitor':
      content = AddVisitor(eventID);
      break;


  }
  showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: color.grey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(67),
          topRight: Radius.circular(67),
        ),
      ),
      context: context,
      builder: (BuildContext context) => content);
}