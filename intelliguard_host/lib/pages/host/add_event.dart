import 'package:flutter/material.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:intelliguard_host/ui/form.dart';
import 'package:intelliguard_host/ui/title.dart';
import 'package:provider/provider.dart';

class AddEvent extends StatefulWidget {
  @override
  _AddEventState createState() => _AddEventState();
}

class _AddEventState extends State<AddEvent> {
  DateTime selectedDateFrom = DateTime.now();
  DateTime selectedDateTo = DateTime.now();
  TimeOfDay selectedTimeFrom = TimeOfDay.now();
  TimeOfDay selectedTimeTo = TimeOfDay.now();
  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _timeController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  List<String> timeFrom;
  List<String> timeTo;
  get intTimeFrom => int.parse(timeFrom[0]);
  get intTimeTo => int.parse(timeTo[0]);
  get formattedTimeFrom =>
      (intTimeFrom > 12 ? intTimeFrom - 12 : timeFrom[0]).toString();
  get formattedTimeTo =>
      (intTimeTo > 12 ? intTimeTo - 12 : timeTo[0]).toString();
  String timeFromFormat;
  String timeToFormat;
  get date =>
      '${selectedDateFrom.toString().split(' ')[0]} - ${selectedDateTo.toString().split(' ')[0]}';
  get time =>
      '${formattedTimeFrom.length < 2 ? '0' + formattedTimeFrom : formattedTimeFrom}:${timeFrom[1]}$timeFromFormat - ${formattedTimeTo.length < 2 ? '0' + formattedTimeTo : formattedTimeTo}:${timeTo[1]}$timeToFormat';
  String eventName;
  String venue;
  String host;
  void _submitForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      final events = Provider.of<EventsModel>(context, listen: false);
      events.addEvent = {
        "name": eventName,
        "time": time,
        "date": date,
        "venue": venue,
        "host": host
      };
      Navigator.pop(context);
    }
  }

  void _getEventName(String input) => eventName = input;
  void _getVenue(String input) => venue = input;
  void _getHost(String input) => host = input;
  Future<void> _selectDateFrom(context) async {
    final DateTime selected = await showDatePicker(
      context: context,
      initialDate: selectedDateFrom,
      firstDate: DateTime(2015),
      lastDate: DateTime(2100),
    );
    if (selected != null)
      setState(() {
        selectedDateFrom = selected;
        _dateController.text = date;
      });
  }

  Future<void> _selectDateTo(context) async {
    final DateTime selected = await showDatePicker(
      context: context,
      initialDate: selectedDateTo,
      firstDate: DateTime(2015),
      lastDate: DateTime(2100),
    );
    if (selected != null)
      setState(() {
        selectedDateTo = selected;
        _dateController.text = date;
      });
  }

  Future<void> _selectTimeFrom(context) async {
    final TimeOfDay selected =
        await showTimePicker(context: context, initialTime: selectedTimeFrom);
    if (selected != null)
      setState(() {
        timeFrom = selected.toString().split('(')[1].split(')')[0].split(':');
        timeFromFormat = int.parse(timeFrom[0]) >= 12 ? 'PM' : 'AM';
        selectedTimeFrom = selected;
        _timeController.text = time;
      });
  }

  Future<void> _selectTimeTo(context) async {
    final TimeOfDay selected =
        await showTimePicker(context: context, initialTime: selectedTimeTo);
    if (selected != null)
      setState(() {
        timeTo = selected.toString().split('(')[1].split(')')[0].split(':');
        timeToFormat = int.parse(timeTo[0]) >= 12 ? 'PM' : 'AM';
        selectedTimeTo = selected;
        _timeController.text = time;
      });
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    return Container(
      height: height * 0.9 + MediaQuery.of(context).viewInsets.bottom,
      padding: EdgeInsets.only(
          top: height * 0.1, bottom: MediaQuery.of(context).viewInsets.bottom),
      margin: EdgeInsets.symmetric(horizontal: width * 0.075),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              FormTitle('New Event'),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                svg: 'assets/images/icon_event_name.svg',
                placeholder: 'Event Name',
                getData: _getEventName,
              ),
              SizedBox(
                height: height * 0.05,
              ),
              GestureDetector(
                onTap: () {
                  _selectDateTo(context);
                  _selectDateFrom(context);
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: AbsorbPointer(
                  child: FormInput(
                    controller: _dateController,
                    svg: 'assets/images/icon_date.svg',
                    placeholder: 'Date',
                    getData: (input) {},
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.05,
              ),
              GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  _selectTimeTo(context);
                  _selectTimeFrom(context);
                },
                child: AbsorbPointer(
                  child: FormInput(
                    controller: _timeController,
                    svg: 'assets/images/icon_time.svg',
                    placeholder: 'Time',
                    getData: (input) {},
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                svg: 'assets/images/icon_map.svg',
                placeholder: 'Venue',
                getData: _getVenue,
              ),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                svg: 'assets/images/icon_profile.svg',
                placeholder: 'Host',
                getData: _getHost,
              ),
              SizedBox(
                height: height * 0.06,
              ),
              Button(
                text: 'SUBMIT',
                handlePress: _submitForm,
              )
            ],
          ),
        ),
      ),
    );
  }
}
