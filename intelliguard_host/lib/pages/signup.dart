import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_host/model/organisation.dart';
import 'package:intelliguard_host/provider_model.dart/organisation_model.dart';
import 'package:provider/provider.dart';
import 'package:intelliguard_host/ui/alert_error.dart';

import '../ui/title.dart';
import '../ui/form.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String _orgName;
  String _orgHeadName;
  String _orgPassword;
  String _orgGuardPassword;
  bool _obscure1 = true;
  bool _obscure2 = true;
  showHidePw1() {
    setState(() {
      _obscure1 = !_obscure1;
    });
  }

  showHidePw2() {
    setState(() {
      _obscure2 = !_obscure2;
    });
  }

  final _formKey = GlobalKey<FormState>();
  void _getOrgName(input) => _orgName = input;
  void _getOrgHeadName(input) => _orgHeadName = input;
  void _getOrgPW(input) => _orgPassword = input;
  void _getGuardPW(input) => _orgGuardPassword = input;
  void _handleSignUp() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      // register in firebase
      final org = Provider.of<OrganisationModel>(context, listen: false);
      org
          .registerOrg(Organisation(
              _orgName, _orgHeadName, _orgPassword, _orgGuardPassword))
          .then((_) {
        if (!_) {
          alertError(context, 'Organisation already exists!').then((_) {
            Navigator.pop(context);
            Navigator.pushReplacementNamed(context, '/');
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(14, 24, 38, 1),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(top: height * 0.05),
            margin: EdgeInsets.symmetric(horizontal: width * 0.075),
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: IconButton(
                    onPressed: () => Navigator.pop(context),
                    icon:
                        SvgPicture.asset('./assets/images/icon_arrow_left.svg'),
                  ),
                ),
                PageTitle('Sign Up'),
                SizedBox(
                  height: height * 0.08,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      FormInput(
                        placeholder: 'Organisation Name',
                        svg: 'assets/images/icon_profile.svg',
                        getData: _getOrgName,
                      ),
                      SizedBox(
                        height: height * 0.06,
                      ),
                      FormInput(
                        placeholder: 'Organisation Head Name',
                        svg: 'assets/images/icon_profile.svg',
                        getData: _getOrgHeadName,
                      ),
                      SizedBox(
                        height: height * 0.06,
                      ),
                      FormInput(
                        placeholder: 'Organisation Password',
                        svg: 'assets/images/icon_lock.svg',
                        getData: _getOrgPW,
                        obscure: _obscure1,
                        showHidePw: showHidePw1,
                      ),
                      SizedBox(
                        height: height * 0.06,
                      ),
                      FormInput(
                        placeholder: 'Guard Password',
                        svg: 'assets/images/icon_lock.svg',
                        getData: _getGuardPW,
                        obscure: _obscure1,
                        showHidePw: showHidePw2,
                      ),
                      SizedBox(
                        height: height * 0.08,
                      ),
                      Button(
                        text: 'SIGNUP',
                        handlePress: _handleSignUp,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
