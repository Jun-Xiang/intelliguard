import 'package:flutter/material.dart';
import 'package:intelliguard_host/model/ui_color.dart';
import 'package:intelliguard_host/pages/add_visitor.dart';


showForm(BuildContext context, eventID) {
  final UiColor color = UiColor();
  showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: color.grey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(67),
          topRight: Radius.circular(67),
        ),
      ),
      context: context,
      builder: (BuildContext context) => AddVisitor(eventID));
}