import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_host/model/org_event.dart';
import 'package:intelliguard_host/model/ui_color.dart';
import 'package:intelliguard_host/pages/worker/show_form.dart';
import 'package:intelliguard_host/pages/worker/view_event.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:intelliguard_host/provider_model.dart/host_worker_model.dart';
import 'package:intelliguard_host/services/auth.dart';
import 'package:intelliguard_host/services/locator.dart';
import 'package:intelliguard_host/ui/alert_text_style.dart';
import 'package:intelliguard_host/ui/list_button.dart';
import 'package:intelliguard_host/ui/list_shape.dart';
import 'package:intelliguard_host/ui/title.dart';
import 'package:provider/provider.dart';

class EventsPage extends StatelessWidget {
  final UiColor color = UiColor();

  Widget _buildContent(BuildContext context, OrgEvent event) {
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 8,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTitle(
                  event.eventName,
                  hasPassed: event.hasPassed,
                ),
                Description(event.host),
              ],
            ),
          ),
          !event.hasPassed
              ? ListButton(
                  svg: './assets/images/icon_add.svg',
                  handlePress: () => showForm(context, event.id))
              : Container(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final events = Provider.of<EventsModel>(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(child: PageTitle('Events')),
            IconButton(
              icon: SvgPicture.asset(
                'assets/images/icon_logout.svg',
                width: 35,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  child: AlertDialog(
                    actions: <Widget>[
                      FlatButton(
                        child: Text(
                          'Cancel',
                          style: buttonStyle,
                        ),
                        onPressed: () => Navigator.pop(context),
                      ),
                      FlatButton(
                          child: Text('Log Out', style: buttonStyle),
                          onPressed: () {
                            final worker = Provider.of<HostWorkerModel>(context,
                                listen: false);
                            final services = locator<Auth>();
                            worker.clearWorker();
                            services.logout().then((_) {
                              Navigator.pop(context);
                              Navigator.pushReplacementNamed(context, '/');
                            });
                          })
                    ],
                    title: Text('Confirm Log Out?', style: titleStyle),
                    content: Text(
                      'This action cannot be undone.',
                      style: contentStyle,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
        SizedBox(
          height: 22,
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
              stream: events.fetchEvents(),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  final docs = snapshot.data.documents;
                  List<OrgEvent> events = docs.map((event) {
                    final data = event.data;
                    return OrgEvent(
                        event.documentID,
                        data['name'],
                        data['time'],
                        data['date'],
                        data['venue'],
                        data['host'],
                        data['hasPassed']);
                  }).toList();
                  return ListView.builder(
                    itemCount: docs.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListShape(
                        _buildContent(context, events[index]),
                        () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ViewEvent(events[index]),
                          ),
                        ),
                      );
                    },
                  );
                } else {
                  return Center(
                      child: SizedBox(
                          height: 100,
                          width: 100,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          )));
                }
              }),
        ),
      ],
    );
  }
}
