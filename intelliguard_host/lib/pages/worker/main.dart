import 'package:flutter/material.dart';
import 'package:intelliguard_host/model/ui_color.dart';
import 'package:intelliguard_host/pages/worker/events_page.dart';

class Worker extends StatefulWidget {
  @override
  _WorkerState createState() => _WorkerState();
}

class _WorkerState extends State<Worker> {
  final UiColor color = UiColor();

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    
    return Scaffold(
        backgroundColor: color.black,
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.only(top: height * 0.05),
            margin: EdgeInsets.symmetric(horizontal: width * 0.075),
            child: EventsPage(),
          ),
        ));
  }
}
