import "package:flutter/material.dart";
import "package:flutter_svg/flutter_svg.dart";

class Button extends StatelessWidget {
  final Function handlePress;
  final String text;
  final Color textColor;
  Button({this.text, this.handlePress, this.textColor});

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return ConstrainedBox(
      constraints: BoxConstraints(
        minWidth: width * 0.85,
        minHeight: height * 0.06 + 16,
      ),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7),
        ),
        onPressed: handlePress,
        child: Text(
          text,
          style: TextStyle(
            fontFamily: 'Pier',
            fontWeight: FontWeight.w700,
            fontSize: 16,
            color: textColor,
          ),
        ),
      ),
    );
  }
}

class ButtonOutline extends StatelessWidget {
  final Function handlePress;
  final String text;
  ButtonOutline({this.text, this.handlePress});

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return ConstrainedBox(
      constraints: BoxConstraints(
        minWidth: width * 0.85,
        minHeight: height * 0.06 + 16,
      ),
      child: RaisedButton(
        color: Colors.transparent,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7),
            side: BorderSide(
              style: BorderStyle.solid,
              color: Theme.of(context).buttonColor,
              width: 2,
            )),
        onPressed: handlePress,
        child: Text(
          text,
          style: TextStyle(
            fontFamily: 'Pier',
            fontWeight: FontWeight.w700,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}

class FormInput extends StatelessWidget {
  final String placeholder;
  final String svg;
  final bool obscure;
  final Function getData;
  final String text;
  final TextEditingController controller;
  final Function validator;
  final Function showHidePw;
  FormInput(
      {this.placeholder,
      this.svg,
      this.obscure = false,
      this.getData,
      this.controller,
      this.text,
      this.showHidePw,
      this.validator});  
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      initialValue: text,
      textAlignVertical: TextAlignVertical.bottom,
      style: TextStyle(
        color: Colors.white,
        fontFamily: 'Pier',
        fontWeight: FontWeight.w500,
      ),
      maxLines: obscure ? 1 : null,
      obscureText: obscure,
      decoration: InputDecoration(
        hintText: placeholder,
        hintStyle: TextStyle(
          fontFamily: 'Pier',
          fontWeight: FontWeight.w500,
        ),
        suffixIcon: showHidePw != null ? FlatButton(child: Icon(Icons.remove_red_eye), onPressed: showHidePw,) : null,
        prefixIcon: svg != null
            ? ConstrainedBox(
                constraints: BoxConstraints(maxHeight: 15, maxWidth: 15),
                child: SvgPicture.asset(
                  svg,
                  fit: BoxFit.scaleDown,
                ),
              )
            : null,
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color.fromRGBO(156, 156, 156, 0.5),
          ),
        ),
      ),
      validator: validator != null ? validator : (value) =>
          value.isEmpty ? 'Please enter the required details!' : null,
      onSaved: (input) => getData(input),
    );
  }
}

class RadioSelection extends StatelessWidget {
  final int group;
  final List<int> values;
  final Function handleChange;
  final String title;

  RadioSelection({this.title, this.group, this.values, this.handleChange});

  Widget _buildRadio(int value) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Radio(
          groupValue: group,
          value: value,
          onChanged: (int value) => handleChange(value),
        ),
        Text(
          value.toString(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(fontFamily: 'Pier', fontSize: 16),
        ),
        Row(
          children: values.map((value) => _buildRadio(value)).toList(),
        ),
      ],
    );
  }
}
