import 'package:flutter/material.dart';

class ListShape extends StatelessWidget {
  final Widget buildContent;
  final Function handleTap;

  ListShape(this.buildContent, this.handleTap);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      height: 76,
      child: Material(
        borderRadius: BorderRadius.circular(10),
        color: Theme.of(context).accentColor,
        child: InkWell(
          borderRadius: BorderRadius.circular(10),
          onTap: handleTap,
          child: buildContent,
        ),
      ),
    );
  }
}
