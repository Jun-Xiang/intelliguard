import 'package:flutter/material.dart';

import '../model/ui_color.dart';

class PageTitle extends StatelessWidget {
  final String title;

  PageTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        color: Colors.white,
        fontFamily: 'Pier',
        fontWeight: FontWeight.w900,
        fontSize: 40,
      ),
    );
  }
}

class EventTitle extends StatelessWidget {
  final String title;

  EventTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        color: Colors.white,
        fontFamily: 'Pier',
        fontWeight: FontWeight.w700,
        fontSize: 25,
      ),
    );
  }
}

class EventDetails extends StatelessWidget {
  final String det;
  final String con;
  final UiColor color = UiColor();

  EventDetails(this.det, this.con);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          det,
          style: TextStyle(
            fontFamily: 'Pier',
            fontWeight: FontWeight.w300,
            fontSize: 18,
            color: color.desc
          ),
        ),
        Text(
          con,
          style: TextStyle(
            fontFamily: 'Pier',
            fontWeight: FontWeight.w300,
            fontSize: 18,
          ),
        ),
      ],
    );
  }
}

class FormTitle extends StatelessWidget {
  final String title;

  FormTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.white,
        fontFamily: 'Pier',
        fontWeight: FontWeight.w700,
        fontSize: 25,
      ),
    );
  }
}

class ListTitle extends StatelessWidget {
  final String title;
  final bool hasPassed;

  ListTitle(this.title, {this.hasPassed = false});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontFamily: 'Pier',
        fontWeight: FontWeight.w700,
        fontSize: 18,
        color: hasPassed ? Colors.red : Colors.white
      ),
    );
  }
}

class Description extends StatelessWidget {
  final String desc;
  final UiColor color = UiColor();

  Description(this.desc);

  @override
  Widget build(BuildContext context) {
    return Text(
      desc,
      overflow: TextOverflow.ellipsis,
      maxLines: 3,
      style: TextStyle(
        fontFamily: 'Pier',
        fontWeight: FontWeight.w300,
        fontSize: 12,
        color: color.placeholder,
      ),
    );
  }
}
