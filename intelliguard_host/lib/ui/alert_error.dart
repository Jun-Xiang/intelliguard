import 'package:flutter/material.dart';

import 'package:intelliguard_host/ui/alert_text_style.dart';

 Future alertError(context, msg) {
    return showDialog(
        context: context,
        child: AlertDialog(
          title: Text(
            'Error!',
            style: titleStyle,
          ),
          content: Text(
            msg,
            style: contentStyle,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'OK',
                style: buttonStyle,
              ),
              onPressed: () => Navigator.pop(context),
            )
          ],
        ));
  }
