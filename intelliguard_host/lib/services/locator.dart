import 'package:get_it/get_it.dart';
import 'package:intelliguard_host/services/api.dart';
import 'package:intelliguard_host/services/auth.dart';

GetIt locator = GetIt.instance;
void setup() {
  locator.registerLazySingleton<Auth>(()=>Auth());
}

void setupOrgLocator(org){
  locator.registerLazySingleton<Api>(()=> Api(org));
}

void resetOrgLocator(){
  locator.unregister<Api>();
}