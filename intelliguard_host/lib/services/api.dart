import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';

class Api {
  final FirebaseDatabase _db = FirebaseDatabase.instance;
  final String org;
  DatabaseReference ref;

  final Firestore _fdb = Firestore.instance;
  var fref;

  Api(this.org) {
    ref = _db.reference().child(org);
    fref = _fdb
        .collection('events_$org')
        .orderBy('date', descending: true)
        .reference();
  }
  // firebase database
  Future<DataSnapshot> getOrgDetails() {
    return ref.once();
  }

  Future<bool> addOrg(org) async {
    final snapshot = await _db.reference().child(org.orgName).once();
    if (snapshot.value == null) {
      _db.reference().child(org.orgName).set({
        "name": org.orgName,
        "password": org.orgPassword,
        "head_worker": org.orgHeadName,
        "guard_password": org.orgGuardPassword,
        "worker": {
          org.orgHeadName: {"isHead": true}
        }
      });
      return true;
    } else {
      return false;
    }
  }

  // updateOrgPw(pw)=>ref.update({"password": pw});
  // updateGuardPw(pw)=>ref.update({"guard_password": pw});
  // updatePw(String path, val) => ref.child(path).update(val);
  Future<bool> updatePw(path, verify, pw) async {
    final snapshot = await getOrgDetails();
    final value = await snapshot.value;
    final oldPw = await value[path];
    if (verify == oldPw) {
      ref.update({path: pw});
      return true;
    }
    return false;
  }

  //only one head
  Future<bool> updateHeadName(verify, name) async {
    final snapshot = await getOrgDetails();
    final value = await snapshot.value;
    final oldHead = await value['head_worker'];
    if (verify == oldHead) {
      ref.child('worker').child(oldHead).remove();
      ref.child('worker').child(name).set({"isHead": true});
      ref.update({"head_worker": name});
      return true;
    }
    return false;
  }

  // addWorkerID(id) =>ref.child('worker').set({id: {"isHead": false}});
  // addGuardID(id) =>ref.child('guard').set({id: {"isHead": false}});
  // addHeadGuardID(id) =>ref.child('guard').set({id: {"isHead": true}});
  Future<void> addID(String path, String id, bool val) {
    return ref.child(path).child(id).set({"isHead": val});
  }

  // deleteWorkerID(id) =>ref.child('worker').child(id).remove();
  // deleteGuard(id) =>ref.child('guard').child(id).remove();
  Future delete(String path) async {
    final res = await ref.child(path).once();
    final isHead = res.value['isHead'];
    if (isHead && path.split('/')[0] == 'worker') {
      return ;
    }
    ref.child(path).remove();
  }

  //firebase firestore

  Future<QuerySnapshot> getDataCollection() => fref.getDocuments();

  Stream<QuerySnapshot> streamDataCollection() {
    return fref.snapshots();
  }

  Stream<QuerySnapshot> getVisitors(String id) {
    return fref.document(id).collection('visitors').snapshots();
  }

  Future<DocumentSnapshot> getEventById(String id) => fref.document(id).get();
  Future<DocumentSnapshot> getVisitorByIC(String eventID, String ic) =>
      fref.document(eventID).collection('visitors').document(ic).get();

  Future<void> updateHasPassedStatus(String eventID) =>
      fref.document(eventID).updateData({"hasPassed": true});

  Future<void> deleteEvent(String id) => fref.document(id).delete();
  Future<void> deleteVisitor(String eventID, String ic) =>
      fref.document(eventID).collection('visitors').document(ic).delete();

  Future<DocumentReference> addEvent(Map event) => fref.add(event);
  Future<void> addVisitor(
          String eventID, String ic, Map<String, dynamic> visitor) =>
      fref
          .document(eventID)
          .collection('visitors')
          .document(ic)
          .setData(visitor);

  //dont need
  // Future<void> updateDocument(Map data, String id) {
  //   return fref.document(id).updateData(data);
  // }
}
