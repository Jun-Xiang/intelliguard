import 'package:flutter/material.dart';
import 'package:intelliguard_host/pages/login.dart';
import 'package:intelliguard_host/provider_model.dart/host_worker_model.dart';
import 'package:intelliguard_host/pages/signup.dart';
import 'package:intelliguard_host/pages/worker/main.dart';
import 'package:intelliguard_host/pages/host/main.dart';
import 'package:intelliguard_host/provider_model.dart/events_model.dart';
import 'package:intelliguard_host/provider_model.dart/organisation_model.dart';
import 'package:intelliguard_host/services/locator.dart';
import 'package:provider/provider.dart';
import 'package:flutter/rendering.dart';

main() {
  setup();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // debugPaintSizeEnabled = true;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => EventsModel(),
        ),
        Provider(
          create: (_) => HostWorkerModel(),
        ),
        Provider(
          create: (_) => OrganisationModel(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primaryColor: Color.fromRGBO(14, 24, 38, 1),
            accentColor: Color.fromRGBO(27, 43, 64, 1),
            hintColor: Color.fromRGBO(135, 135, 135, 1),
            buttonColor: Color.fromRGBO(99, 161, 249, 1),
            brightness: Brightness.dark,
            inputDecorationTheme: InputDecorationTheme(
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color.fromRGBO(135, 135, 135, 0.5),
                ),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color.fromRGBO(135, 135, 135, 1),
                ),
              ),
            ),
          ),
          home: Login(),
          routes: {
            'host': (BuildContext context) => Host(),
            'worker': (BuildContext context) => Worker(),
            'signup': (BuildContext context) => SignUp()
          }),
    );
  }
}
