class Organisation {
  final String orgName;
  final String orgHeadName;
  final String orgPassword;
  final String orgGuardPassword;

  Organisation(this.orgName, this.orgHeadName, this.orgPassword, this.orgGuardPassword);

}