class Visitor {
  final String name;
  final String ic;
  final String age;
  final String email;
  final bool present;

  Visitor(this.name, this.ic, this.age, this.email, this.present);
}