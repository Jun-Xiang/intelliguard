class HostWorker {
  final String id;
  final bool isHost;

  HostWorker(this.id, this.isHost);

}