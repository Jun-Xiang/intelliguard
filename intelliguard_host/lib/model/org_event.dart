import 'package:intelliguard_host/model/visitor.dart';

class OrgEvent {
  final String id;
  final String eventName;
  final String time;
  final String date;
  final String venue;
  final String host;
  final List<Visitor> visitors = [];
  final bool hasPassed;

  OrgEvent(
      this.id, this.eventName, this.time, this.date, this.venue, this.host, this.hasPassed);
}
