import 'package:intelliguard_host/model/host_worker.dart';
import 'package:intelliguard_host/services/api.dart';
import 'package:intelliguard_host/services/locator.dart';

class HostWorkerModel {
  HostWorker _worker;
  Api _api = locator<Api>();

  get worker => _worker;
  Future<bool> add(id) async {
    final snapshot = await _api.getOrgDetails();
    final val = await snapshot.value;
    final workerId = [];
    val['worker'].forEach((i, x) {
      workerId.add(i);
    });
    if (!workerId.contains(id)) throw ('No ID found!');
    String head = val['head_worker'];
    bool isHead = head == id;
    _worker = HostWorker(id, isHead);
    return isHead;
  }

  clearWorker() {
    resetOrgLocator();
    _worker = null;
  }
}
