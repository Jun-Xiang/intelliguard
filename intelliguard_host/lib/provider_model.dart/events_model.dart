import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:intelliguard_host/services/api.dart';
import 'package:intelliguard_host/services/locator.dart';
import 'package:http/http.dart' as http;

class EventsModel with ChangeNotifier {
  Api _api = locator<Api>();

  Stream<QuerySnapshot> fetchEvents() {
    return _api.streamDataCollection();
  }

  Stream<QuerySnapshot> loadVisitors(String id) {
    return _api.getVisitors(id);
  }

  Future<Null> checkForPassedEvents() async {
    final snapshot = await _api.getDataCollection();
    snapshot.documents.forEach((doc) {
      if (!doc.data['hasPassed']) {
        final dateAry = doc.data['date'].split('-');
        final endTime = doc.data['time'].split('-')[1];
        final endTimeNoFormat =
            endTime.substring(0, endTime.length - 2).split(':');
        final endHour = endTime.contains('PM')
            ? int.parse(endTimeNoFormat[0]) + 12
            : int.parse(endTimeNoFormat[0]);
        final endMin = int.parse(endTimeNoFormat[1]);
        final endYr = int.parse(dateAry[3]);
        final endMn = int.parse(dateAry[4]);
        final endDy = int.parse(dateAry[5]);
        final end = DateTime(endYr, endMn, endDy, endHour, endMin);
        final current = DateTime.now();
        if (current.difference(end).inMinutes >= 0) {
          _api.updateHasPassedStatus(doc.documentID);
        }
      }
    });
  }

  set addEvent(event) {
    _api.addEvent(event);
  }

  void deleteEvent(id) => _api.deleteEvent(id);

  void deleteVisitor(eventID, ic) => _api.deleteVisitor(eventID, ic);

  Future<bool> addVisitor(eventID, visitor) async {
    final snapshot = await _api.getVisitorByIC(eventID, visitor.ic);
    if (snapshot.exists) {
      return false;
    } else {
      final event = await _api.getEventById(eventID);
      final eventDetails = event.data;
      final Map<String, dynamic> body = {
        'ic': visitor.ic,
        'email': visitor.email,
        'date': eventDetails['date'],
        'time': eventDetails['time'],
        'name': eventDetails['name'],
        'venue': eventDetails['venue'],
        'org': _api.org,
        'event_id': eventID
      };
      http.post('https://dry-reef-16429.herokuapp.com/sendEmail',
          body: json.encode(body),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
          });
      Map<String, dynamic> visitorDetails = {
        "name": visitor.name,
        "age": visitor.age,
        "email": visitor.email,
        "present": visitor.present
      };
      _api.addVisitor(eventID, visitor.ic, visitorDetails);
      return true;
    }
  }
}
