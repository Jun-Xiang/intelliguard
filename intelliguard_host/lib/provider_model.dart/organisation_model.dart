
import 'package:intelliguard_host/model/organisation.dart';
import 'package:intelliguard_host/services/api.dart';
import 'package:intelliguard_host/services/locator.dart';

class OrganisationModel {
  Api _api = locator<Api>();

  Organisation _org;

  void loadOrg() async{
    final snapshot = await _api.getOrgDetails();
    final val = await snapshot.value;
     _org = Organisation(val['org'], val['head_worker'], val['password'], val['guard_password']);
  }

  Future<bool> registerOrg(Organisation org) =>_api.addOrg(org);

  get guardPw => _org.orgGuardPassword;
  get headName => _org.orgHeadName;
  get orgPw => _org.orgPassword;

  set orgPw(pw) => _org = Organisation(_org.orgName, _org.orgHeadName, pw,
      _org.orgGuardPassword);
  set headName(name) => _org = Organisation(_org.orgName, name, _org.orgPassword,
      _org.orgGuardPassword);
  set guardPw(pw) => _org = Organisation(_org.orgName, _org.orgHeadName,
      _org.orgPassword, pw);
}
