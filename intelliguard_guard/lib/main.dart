import 'package:flutter/material.dart';
import 'package:intelliguard_guard/pages/login.dart';
import 'package:intelliguard_guard/provider_model/guard_model.dart';
import 'package:intelliguard_guard/provider_model/problems_model.dart';
import 'package:intelliguard_guard/provider_model/tickets_model.dart';
import 'package:intelliguard_guard/provider_model/visitors_model.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:provider/provider.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:flutter/rendering.dart'; 
import 'pages/head_guard/main.dart';
import 'pages/guard/main.dart';


main() {
  setup();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    debugPaintSizeEnabled = true;
    return MultiProvider(
      providers: [
        Provider(create: (_) => VisitorsModel()),
        Provider(create: (_) => TicketsModel()),
        Provider(
          create: (_) => GuardModel(),
        ),
        Provider(create: (_) => ProblemsModel()),
        Provider(
          create: (_) => CurrentEventModel(),
        )
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primaryColor: Color.fromRGBO(14, 24, 38, 1),
            accentColor: Color.fromRGBO(27, 43, 64, 1),
            hintColor: Color.fromRGBO(135, 135, 135, 1),
            buttonColor: Color.fromRGBO(99, 161, 249, 1),
            brightness: Brightness.dark,
            inputDecorationTheme: InputDecorationTheme(
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color.fromRGBO(135, 135, 135, 0.5),
                ),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Color.fromRGBO(135, 135, 135, 1),
                ),
              ),
            ),
          ),
          home: Login(),
          routes: {
            'headguard': (BuildContext context) => HeadGuardPage(),
            'guard': (BuildContext context) => GuardPage(),
          }),
    );
  }
}
