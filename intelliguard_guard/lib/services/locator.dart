import 'package:get_it/get_it.dart';
import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/auth.dart';
import 'package:intelliguard_guard/services/show_notifications.dart';

GetIt locator = GetIt.instance;
void setup() {
  locator.registerLazySingleton<Auth>(()=>Auth());
}

void setupOrgLocator(org){
  locator.registerLazySingleton<Api>(()=> Api(org));
}

void setupNotificationLocator(showNotification){
  locator.registerLazySingleton<ShowNotification>(()=>ShowNotification(showNotification));
}

void resetOrgLocator(){
  locator.unregister<Api>();
  locator.unregister<ShowNotification>();
}