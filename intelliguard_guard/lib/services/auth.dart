import 'package:firebase_database/firebase_database.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Auth {
  final dbRef = FirebaseDatabase.instance.reference();

  Future<bool> orgExists(org) async {
    final ref = await dbRef.child(org).once();
    bool exists = await ref.value != null;
    return exists;
  }

  Future<String> orgPassword(org)async{
    final ref = await dbRef.child(org + '/guard_password').once();
    final String pw = await ref.value;
    return pw;
  }

  registerOrg(org){
    setupOrgLocator(org);
  }

   Future<Null> login(org, guardId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('org', org);
    prefs.setString('id', guardId);
  }

  Future<Null> logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('org');
    prefs.remove('id');
  }

  Future<bool> isLoggedIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final bool org = prefs.containsKey('org');
    final bool id = prefs.containsKey('id');
    if(org && id){
      registerOrg(prefs.getString('org'));
    }
    return (org && id);  
  }

  Future<String> getID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('id');
  }
}
