import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';


class Api {
  final FirebaseDatabase _db = FirebaseDatabase.instance;
  final String org;
  DatabaseReference ref;

  final Firestore _fdb = Firestore.instance;
  CollectionReference fref;

  Api(this.org) {
    ref = _db.reference().child(org);
    fref = _fdb.collection('events_' + org).orderBy('date', descending: true).reference();
  }
  // firebase database
  Future<DataSnapshot> getOrgDetails() {
    return ref.once();
  }

  Future<void> addTicketIDToGuard(guardID, ticketID) {
    return ref.child('guard/$guardID/ticketID/$ticketID').set(true);
  }
  
  Future<void> removeTicketIDFromGuard(guardID, ticketID) {
    return ref.child('guard/$guardID/ticketID/$ticketID').remove();
  }

  //firebase firestore

  Future<QuerySnapshot> getDataCollection() => fref.getDocuments();

  Stream<QuerySnapshot> streamDataCollection() {
    return fref.snapshots();
  }

  Stream<QuerySnapshot> getVisitors(String id) {
    return fref.document(id).collection('visitors').snapshots();
  }
  Stream<QuerySnapshot> getTickets(String id) {
    return fref.document(id).collection('tickets').snapshots();
  }
  Stream<QuerySnapshot> getProblems(String id) {
    return fref.document(id).collection('problems').snapshots();
  }
  Future<DocumentSnapshot> getDocumentById(String id) =>
      fref.document(id).get();

  Future<void> updateTicketStatus(String eventID, String id, String status) => fref.document(eventID).collection('tickets').document(id).updateData({"status": status});
  Future<void> updateProblemStatus(String eventID, String id, bool reported) => fref.document(eventID).collection('problems').document(id).updateData({"reported": reported});

  Future<void> deleteEvent(String id) => fref.document(id).delete();
  Future<void> deleteTicket(String eventID, String ticketID) =>
      fref.document(eventID).collection('tickets').document(ticketID).delete();

  Future<DocumentReference> addEvent(Map event) => fref.add(event);
  Future<DocumentReference> addTicket(String eventID, Map<String, dynamic> ticket) => fref.document(eventID).collection('tickets').add(ticket);
  Future<DocumentReference> addProblem(String eventID, Map<String, dynamic> problem) => fref.document(eventID).collection('problems').add(problem);
  
  Future<void> addVisitor(
          String eventID, String ic, Map<String, dynamic> visitor) =>
      fref
          .document(eventID)
          .collection('visitors')
          .document(ic)
          .setData(visitor);

  //dont need
  // Future<void> updateDocument(Map data, String id) {
  //   return fref.document(id).updateData(data);
  // }

  //firebase storage

  Future<String> getImageURL(path) async {
    FirebaseStorage _storage = FirebaseStorage(storageBucket: 'gs://intelliguard-baf01.appspot.com');
    return await _storage.ref().child(path).getDownloadURL();
  }
}
