import 'package:flutter/material.dart';

final TextStyle buttonStyle =
      TextStyle(fontFamily: 'Pier', fontWeight: FontWeight.w500, fontSize: 18);
  final TextStyle titleStyle =
      TextStyle(fontFamily: 'Pier', fontWeight: FontWeight.w700, fontSize: 25);
  final TextStyle contentStyle =
      TextStyle(fontFamily: 'Pier', fontWeight: FontWeight.w300, fontSize: 15);