import "package:flutter/material.dart";
import "package:flutter_svg/flutter_svg.dart";

class Button extends StatelessWidget {
  final Function handlePress;
  final String text;
  Button({this.text, this.handlePress});

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return ConstrainedBox(
      constraints: BoxConstraints(
        minWidth: width * 0.85,
        minHeight: height * 0.06 + 16,
      ),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7),
        ),
        onPressed: handlePress,
        child: Text(
          text,
          style: TextStyle(
              fontFamily: 'Pier', fontWeight: FontWeight.w700, fontSize: 16),
        ),
      ),
    );
  }
}

class FormInput extends StatelessWidget {
  final String placeholder;
  final String svg;
  final bool obscure;
  final Function getData;
  final Function showHidePw;
  final String text;
  FormInput(
      {this.placeholder,
      this.svg,
      this.obscure = false,
      this.getData,
      this.showHidePw,
      this.text});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: text == null ? '' : text,
      textAlignVertical: TextAlignVertical.bottom,
      style: TextStyle(
        color: Colors.white,
        fontFamily: 'Pier',
        fontWeight: FontWeight.w500,
      ),
      maxLines: obscure ? 1 : null,
      obscureText: obscure,
      decoration: InputDecoration(
        suffixIcon: showHidePw != null
            ? FlatButton(
                child: Icon(Icons.remove_red_eye),
                onPressed: showHidePw,
              )
            : null,
        hintText: placeholder,
        hintStyle: TextStyle(
          fontFamily: 'Pier',
          fontWeight: FontWeight.w500,
        ),
        prefixIcon: svg != null
            ? ConstrainedBox(
                constraints: BoxConstraints(maxHeight: 15, maxWidth: 15),
                child: SvgPicture.asset(
                  svg,
                  fit: BoxFit.scaleDown,
                ),
              )
            : null,
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color.fromRGBO(156, 156, 156, 0.5),
          ),
        ),
      ),
      validator: (value) =>
          value.trim().isEmpty ? 'Please enter the required details!' : null,
      onSaved: (input) => getData(input),
    );
  }
}

class RadioSelection extends StatelessWidget {
  final int group;
  final List<int> values;
  final Function handleChange;
  final String title;

  RadioSelection({this.title, this.group, this.values, this.handleChange});

  Widget _buildRadio(int value) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Radio(
          groupValue: group,
          value: value,
          onChanged: (int value) => handleChange(value),
        ),
        Text(
          value.toString(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(fontFamily: 'Pier', fontSize: 16),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: values.map((value) => _buildRadio(value)).toList(),
        ),
      ],
    );
  }
}
