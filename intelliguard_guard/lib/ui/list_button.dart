import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../model/ui_color.dart';

class ListButton extends StatelessWidget {
  final UiColor color = UiColor();
  final String svg;
  final Function handlePress;
  final String desc;
  final String level;
  final String title;

  ListButton({this.svg, this.handlePress, this.desc, this.level, this.title});
  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 2,
      child: SizedBox(
        width: 40,
        height: 40,
        child: FlatButton(
          color: color.button,
          padding: EdgeInsets.all(0),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: SvgPicture.asset(
            svg,
            width: 13,
            fit: BoxFit.scaleDown,
          ),
          onPressed: handlePress,
        ),
      ),
    );
  }
}
