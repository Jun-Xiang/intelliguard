class Organisation {
  final String orgName;
  final String orgPassword;
  final String orgWorkerPassword;
  final String orgGuardPassword;
  final List<String> eventsId;

  Organisation(this.orgName, this.orgPassword, this.orgWorkerPassword, this.orgGuardPassword, this.eventsId);

}