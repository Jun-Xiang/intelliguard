class Problem {
  final String id;
  final String reporterId;
  final String place;
  final int level;
  final String desc;
  final bool isReported;
  final imagePath;

  Problem(this.id, this.reporterId, this.place, this.level, this.desc, this.isReported, this.imagePath);
}