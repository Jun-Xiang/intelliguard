class Guard {
  final String id;
  final List<String> ticketId;
  final bool isHeadGuard;

  Guard(this.id, this.ticketId, this.isHeadGuard);

}