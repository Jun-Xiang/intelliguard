class Ticket {
  final String id;
  final String place;
  final int level;
  final String status;
  final String desc;
  final imagePath;

  Ticket(this.id, this.place, this.level, this.status, this.desc, this.imagePath);

}
