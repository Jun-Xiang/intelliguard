class Visitor {
  final String name;
  final String ic;
  final String age;
  final bool present;

  Visitor(this.name, this.ic, this.age, this.present);
}