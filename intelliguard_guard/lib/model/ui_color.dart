import 'package:flutter/material.dart';

class UiColor {
  Map<String, Color> labelColor = {
    'red': Color.fromRGBO(230, 52, 98, 1),
    'green': Color.fromRGBO(93, 190, 112, 1),
    'orange': Color.fromRGBO(254, 95, 85, 1),
    'grey' : Color.fromRGBO(47, 67, 93, 1),
    'accentColor' : Color.fromRGBO(27, 43, 64, 1),
    'black' : Color.fromRGBO(14, 24, 38, 1),
    'placeholder' : Color.fromRGBO(159, 159, 159, 1),
    'button' : Color.fromRGBO(99, 161, 249, 1)
  };

  Color get red {
    return labelColor['red'];
  }

  Color get green {
    return labelColor['green'];
  }

  Color get orange {
    return labelColor['orange'];
  }
  Color get grey {
    return labelColor['grey'];
  }

  Color get black {
    return labelColor['black'];
  }

  Color get placeholder {
    return labelColor['placeholder'];
  }

  Color get button {
    return labelColor['button'];
  }
  
  Color get accentColor {
    return labelColor['accentColor'];
  }

}
