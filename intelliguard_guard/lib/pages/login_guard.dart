import 'package:flutter/material.dart';
import 'package:intelliguard_guard/provider_model/guard_model.dart';
import 'package:intelliguard_guard/services/auth.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/ui/alert_error.dart';
import 'package:intelliguard_guard/ui/form.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';
import 'package:intelliguard_guard/pages/choose_event.dart';

class LoginGuard extends StatefulWidget {
  final String org;
  LoginGuard(this.org);
  @override
  _LoginGuardState createState() => _LoginGuardState();
}

class _LoginGuardState extends State<LoginGuard> {
  final _formKey = GlobalKey<FormState>();
  final services = locator<Auth>();
  void _getData(String input) {
    String page;
    final guard = Provider.of<GuardModel>(context, listen: false);
    guard.addGuard(input).then((_) async{
      if (_)
        page = 'headguard';
      else
        page = 'guard';
      final events = await guard.loadCurrentEvent(); 
      services.login(widget.org, input);
      Navigator.pop(context);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_)=> ChooseEvent(events, page)));
    }, onError: (error){
      alertError(context, error);
    });
  }

  void _handlePress() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.only(top: height * 0.05),
      margin: EdgeInsets.symmetric(horizontal: width * 0.075),
      height: height * 0.4 + MediaQuery.of(context).viewInsets.bottom,
      child: Column(
        children: <Widget>[
          FormTitle('Enter ID'),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: height * 0.02,
                ),
                FormInput(
                  svg: './assets/images/icon_profile.svg',
                  placeholder: 'ID',
                  getData: _getData,
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                Button(
                  text: 'LOGIN',
                  handlePress: _handlePress,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
