import 'package:flutter/material.dart';
import 'package:intelliguard_guard/model/problem.dart';

import '../../model/ui_color.dart';
import 'ticket_form.dart';

showForm(BuildContext context, {Problem problem}) {
  final UiColor color = UiColor();
  showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: color.grey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(67),
          topRight: Radius.circular(67),
        ),
      ),
      context: context,
      builder: (BuildContext context) => TicketForm(problem: problem));
}
