import 'package:flutter/material.dart';
import 'package:intelliguard_guard/model/ticket.dart';
import 'package:intelliguard_guard/model/ui_color.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:intelliguard_guard/provider_model/tickets_model.dart';
import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/ui/label.dart';
import 'package:intelliguard_guard/ui/list_button.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';

class TicketList extends StatefulWidget {
  final Ticket ticket;

  TicketList(this.ticket);
  @override
  TicketListState createState() => TicketListState();
}

class TicketListState extends State<TicketList> {
  UiColor color = UiColor();
  Color _levelColor;
  Color _statusColor;

  @override
  Widget build(BuildContext context) {
    Api _api = locator<Api>();
    final tickets = Provider.of<TicketsModel>(context, listen: false);
    final event = Provider.of<CurrentEventModel>(context, listen: false);
    final ticket = widget.ticket;
    bool _showDeleteButton = false;

    switch (ticket.level) {
      case 1:
        _levelColor = color.green;
        break;
      case 2:
        _levelColor = color.orange;
        break;
      case 3:
        _levelColor = color.red;
        break;
    }

    switch (ticket.status) {
      case 'SOLVED':
        _statusColor = color.green;
        break;
      case 'SOLVING':
        _statusColor = color.orange;
        break;
      case 'UNSOLVED':
        _statusColor = color.red;
        _showDeleteButton = true;
        break;
    }

    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        ListTitle(ticket.place),
                        Label(
                          label: 'LEVEL ${ticket.level}',
                          color: _levelColor,
                        ),
                        Label(label: ticket.status, color: _statusColor)
                      ],
                    ),
                    Description(ticket.desc),
                  ],
                ),
              ),
              _showDeleteButton
                  ? ListButton(
                      svg: 'assets/images/icon_dash.svg',
                      handlePress: () {
                        tickets.removeTicket(
                            event.snapshot.documentID, ticket.id);
                      })
                  : Flexible(
                      flex: _showDeleteButton ? 0 : 2,
                      child: Container(),
                    )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          FutureBuilder(
              future: _api.getImageURL(ticket.imagePath),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Image.network(
                    snapshot.data,
                    loadingBuilder:
                        (context, child, ImageChunkEvent loadingProgress) {
                      if (loadingProgress == null) return child;
                      return CircularProgressIndicator(
                        backgroundColor: Colors.white,
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes
                            : null,
                      );
                    },
                  );
                } else {
                  return CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  );
                }
              })
        ],
      ),
    );
  }
}
