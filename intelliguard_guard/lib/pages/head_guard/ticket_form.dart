import 'package:flutter/material.dart';
import 'package:intelliguard_guard/model/problem.dart';
import 'package:intelliguard_guard/pages/camera.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:intelliguard_guard/provider_model/problems_model.dart';
import 'package:intelliguard_guard/provider_model/tickets_model.dart';
import 'package:intelliguard_guard/ui/form.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';

class TicketForm extends StatefulWidget {
  final Problem problem;
  TicketForm({this.problem});
  @override
  _TicketFormState createState() => _TicketFormState();
}

class _TicketFormState extends State<TicketForm> {
  int group = 1;
  var problems;
  Problem problem;
  var place;
  var desc;
  var imagePath;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    problems = Provider.of<ProblemsModel>(context, listen: false);
    problem = widget.problem;
    if (problem != null) {
      place = problem.place;
      desc = problem.desc;
      imagePath = problem.imagePath;
    }
    group = problem != null ? problem.level : 1;
    super.initState();
  }

  void _handleRadioChange(int value) {
    setState(() {
      group = value;
    });
  }

  void _getTitleData(String input) => place = input;

  void _getDescData(String input) => desc = input;

  void _submitForm(path) {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      final problems = Provider.of<ProblemsModel>(context, listen: false);
      final tickets = Provider.of<TicketsModel>(context, listen: false);
      final event = Provider.of<CurrentEventModel>(context, listen: false);
      if (widget.problem != null)
        problems.reported(event.snapshot.documentID, problem.id);
      Map<String, dynamic> ticket = {
        "place": place,
        "level": group,
        "status": 'UNSOLVED',
        "desc": desc,
        "image_path": path
      };
      tickets.addTicket(event.snapshot.documentID, ticket);
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    return Container(
      height: height * 0.8 + MediaQuery.of(context).viewInsets.bottom,
      padding: EdgeInsets.only(
          top: height * 0.1, bottom: MediaQuery.of(context).viewInsets.bottom),
      margin: EdgeInsets.symmetric(horizontal: width * 0.075),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              FormTitle('New Ticket'),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                svg: 'assets/images/icon_place.svg',
                placeholder: 'Place',
                getData: _getTitleData,
                text: place,
              ),
              SizedBox(
                height: height * 0.05,
              ),
              RadioSelection(
                group: group,
                title: 'Severity',
                values: [1, 2, 3],
                handleChange: _handleRadioChange,
              ),
              SizedBox(
                height: height * 0.05,
              ),
              FormInput(
                placeholder: 'Description',
                text: desc,
                getData: _getDescData,
              ),
              SizedBox(
                height: height * 0.05,
              ),
              Camera(_submitForm, imagePath: imagePath,)
            ],
          ),
        ),
      ),
    );
  }
}
