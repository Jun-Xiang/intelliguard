import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_guard/model/ui_color.dart';
import 'package:intelliguard_guard/pages/back_to_choose.dart';
import 'package:intelliguard_guard/pages/head_guard/problems_page.dart';
import 'package:intelliguard_guard/pages/head_guard/show_form.dart';
import 'package:intelliguard_guard/pages/head_guard/tickets_page.dart';
import 'package:intelliguard_guard/pages/settings.dart';
import 'package:intelliguard_guard/pages/visitors_page.dart';
import 'package:intelliguard_guard/provider_model/guard_model.dart';
import 'package:intelliguard_guard/services/auth.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/ui/alert_text_style.dart';
import 'package:provider/provider.dart';

class HeadGuardPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HeadGuardPageState();
  }
}

class _HeadGuardPageState extends State<HeadGuardPage> {
  final UiColor color = UiColor();

  Map<int, String> nav = {
    1: './assets/images/icon_visitors.svg',
    2: './assets/images/icon_ticket.svg',
    3: './assets/images/icon_problems.svg',
    4: './assets/images/icon_choose.svg',
    5: './assets/images/icon_logout.svg',
  };
  Map<int, bool> selected = {
    1: true,
    2: false,
    3: false,
    4: false,
    5: false,
  };
  List<Widget> _buildNav(double height, double width, Function handlePress) {
    List<Widget> navItems = [];
    nav.forEach((int i, String s) {
      navItems.add(SizedBox(
        width: width * 0.15,
        height: width * 0.15,
        child: FlatButton(
          padding: EdgeInsets.all(0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(13)),
          onPressed: () { if(i == 5)_buildLogOutDialog();else if(i == 4) backToChoose(context); else handlePress(i);},
          splashColor: color.black,
          focusColor: color.black,
          color: selected[i] ? color.black : null,
          highlightColor: color.black,
          child: SvgPicture.asset(
            s,
            width: width * 0.08,
          ),
        ),
      ));
    });
    return navItems.toList();
  }

  void _handlePress(int pos) {
    setState(() {
      selected.updateAll((i, b) => selected[i] = false);
      selected[pos] = true;
    });
  }

  _buildLogOutDialog() {
    final guard = Provider.of<GuardModel>(context, listen: false);
    var dialog = AlertDialog(
      actions: <Widget>[
        FlatButton(
          child: Text(
            'Cancel',
            style: buttonStyle,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        FlatButton(
            child: Text('Log Out', style: buttonStyle),
            onPressed: () {
              guard.clearGuard().then((_) {
                final services = locator<Auth>();
                services.logout().then((_) {
                  Navigator.pop(context);
                  Navigator.pushReplacementNamed(context, '/');
                });
              });
            })
      ],
      title: Text('Confirm Log Out?', style: titleStyle),
      content: Text(
        'This action cannot be undone.',
        style: contentStyle,
      ),
    );
    if (guard.guard.ticketId.isNotEmpty)
      dialog = AlertDialog(
        title: Text('You have unfinished ticket!', style: titleStyle),
        content: Text(
            'It seems like you have accepted a ticket and haven\'t finish it.',
            style: contentStyle),
        actions: <Widget>[
          FlatButton(
            child: Text('OK', style: buttonStyle),
            onPressed: () => Navigator.pop(context),
          )
        ],
      );
    showDialog(
      context: context,
      child: dialog,
    );
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    Widget content;
    int selectedNum;

    selected.forEach(
        (int i, bool b) => selected[i] == true ? selectedNum = i : null);
    switch (selectedNum) {
      case 1:
        content = VisitorsPage();
        break;
      case 2:
        content = TicketsPage();
        break;
      case 3:
        content = ProblemsPage();
        break;
      case 4:
        content = SettingsPage();
        break;
    }

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => showForm(context),
        backgroundColor: Theme.of(context).buttonColor,
        child: SvgPicture.asset('./assets/images/icon_add.svg'),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: color.grey),
        height: height * 0.1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: _buildNav(height, width, _handlePress),
        ),
      ),
      backgroundColor: color.black,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(top: height * 0.05),
          margin: EdgeInsets.symmetric(horizontal: width * 0.075),
          child: content,
        ),
      ),
    );
  }
}
