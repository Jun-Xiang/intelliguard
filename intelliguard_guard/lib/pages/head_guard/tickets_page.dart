import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intelliguard_guard/model/ticket.dart';
import 'package:intelliguard_guard/model/ui_color.dart';
import 'package:intelliguard_guard/pages/head_guard/ticket_list.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:intelliguard_guard/provider_model/tickets_model.dart';
import 'package:intelliguard_guard/ui/list_shape.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';

class TicketsPage extends StatelessWidget {
  final UiColor color = UiColor();

  @override
  Widget build(BuildContext context) {
    final tickets = Provider.of<TicketsModel>(context, listen:false);
    final event = Provider.of<CurrentEventModel>(context, listen:false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        PageTitle('Tickets'),
        SizedBox(
          height: 22,
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
              stream: tickets.loadTicket(event.snapshot.documentID),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final data = snapshot.data;
                  final docs = data.documents;
                  List<Ticket> tickets = docs.map((doc) {
                    return Ticket(doc.documentID, doc['place'], doc['level'],
                        doc['status'], doc['desc'], doc['image_path']);
                  }).toList();
                  return ListView.builder(
                    itemCount: tickets.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListShape(TicketList(tickets[index]));
                    },
                  );
                } else {
                  return Center(
                      child: SizedBox(
                          height: 100,
                          width: 100,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          )));
                }
              }),
        ),
      ],
    );
  }
}
