import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intelliguard_guard/model/problem.dart';
import 'package:intelliguard_guard/model/ui_color.dart';
import 'package:intelliguard_guard/pages/head_guard/show_form.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:intelliguard_guard/provider_model/problems_model.dart';
import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/ui/label.dart';
import 'package:intelliguard_guard/ui/list_button.dart';
import 'package:intelliguard_guard/ui/list_shape.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';

class ProblemsPage extends StatelessWidget {
  final UiColor color = UiColor();

  Widget _buildContent(Problem problem, BuildContext context) {
    Api _api = locator<Api>();
    Color levelColor;
    switch (problem.level) {
      case 1:
        levelColor = color.green;
        break;
      case 2:
        levelColor = color.orange;
        break;
      case 3:
        levelColor = color.red;
        break;
    }
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        ListTitle(problem.place),
                        Label(
                          label: 'LEVEL ${problem.level}',
                          color: levelColor,
                        ),
                      ],
                    ),
                    ReportedBy(problem.reporterId),
                    Description(problem.desc)
                  ],
                ),
              ),
              !problem.isReported
                  ? ListButton(
                      svg: './assets/images/icon_arrow_right.svg',
                      handlePress: () => showForm(context, problem: problem))
                  : Flexible(
                      flex: 2,
                      child: Container(),
                    )
            ],
          ),
          SizedBox(height: 20,),
         FutureBuilder(
              future: _api.getImageURL(problem.imagePath),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Image.network(
                    snapshot.data,
                    loadingBuilder:
                        (context, child, ImageChunkEvent loadingProgress) {
                      if (loadingProgress == null) return child;
                      return CircularProgressIndicator(
                        backgroundColor: Colors.white,
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes
                            : null,
                      );
                    },
                  );
                } else {
                  return CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  );
                }
              })
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final problems = Provider.of<ProblemsModel>(context, listen:false);
    final event = Provider.of<CurrentEventModel>(context, listen: false);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        PageTitle('Problems'),
        SizedBox(
          height: 22,
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
              stream: problems.loadProblem(event.snapshot.documentID),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final data = snapshot.data;
                  final docs = data.documents;
                  List<Problem> problems = docs.map((doc){
                    return Problem(doc.documentID, doc['reported_by'], doc['place'], doc['level'], doc['desc'], doc['reported'], doc['image_path']);
                  }).toList();
                  return ListView.builder(
                    itemCount: problems.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListShape(_buildContent(
                        problems[index],
                        context,
                      ));
                    },
                  );
                } else {
                  return Center(
                      child: SizedBox(
                          height: 100,
                          width: 100,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          )));
                }
              }),
        ),
      ],
    );
  }
}
