import 'package:flutter/material.dart';
import 'package:intelliguard_guard/services/auth.dart';
import 'package:intelliguard_guard/services/locator.dart';

import '../ui/title.dart';
import '../ui/form.dart';

class SettingsPage extends StatelessWidget {
  void _handlePress(context) {
    Navigator.pushReplacementNamed(context, '/');
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        PageTitle('Settings'),
        Button(
          text: 'LOGOUT',
          handlePress: () {
            final services = locator<Auth>();
            services.logout().then((_) {
              _handlePress(context);
            });
          },
        )
      ],
    );
  }
}
