import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:intelliguard_guard/provider_model/guard_model.dart';
import 'package:intelliguard_guard/provider_model/problems_model.dart';
import 'package:intelliguard_guard/provider_model/tickets_model.dart';
import 'package:intelliguard_guard/services/auth.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/ui/form.dart';
import 'package:intelliguard_guard/ui/list_shape.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';

class ChooseEvent extends StatelessWidget {
  final List<DocumentSnapshot> events;
  final String page;

  ChooseEvent(this.events, this.page);

  Widget _buildContent(BuildContext context, DocumentSnapshot event) {
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 8,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTitle(event['name']),
                Description(event['host']),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final event = Provider.of<CurrentEventModel>(context, listen: false);
    final tickets = Provider.of<TicketsModel>(context, listen: false);
    final problems = Provider.of<ProblemsModel>(context, listen: false);
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color.fromRGBO(14, 24, 38, 1),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(top: height * 0.05),
          margin: EdgeInsets.symmetric(horizontal: width * 0.075),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              PageTitle('Choose Event'),
              SizedBox(
                height: 22,
              ),
              if (events.length != 0)
                Expanded(
                  child: ListView.builder(
                      itemCount: events.length,
                      itemBuilder: (_, index) {
                        return ListShape(_buildContent(context, events[index]),
                            handleTap: () {
                          event.setEvent = events[index];
                          tickets.startEvent(
                              event.snapshot.documentID, page == 'headguard');
                          problems.startEvent(
                              event.snapshot.documentID, page == 'headguard');
                          Navigator.pushReplacementNamed(context, page);
                        });
                      }),
                ),
              if (events.length == 0)
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SvgPicture.asset(
                        'assets/images/illustration_no_events.svg',
                        width: width * 0.85,
                      ),
                      Text(
                        'No event is happening...',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: 'Pier',
                            fontWeight: FontWeight.w500,
                            fontSize: 30),
                      ),
                      SizedBox(height: 20,),
                      Button(
                          text: 'Log Out',
                          handlePress: () {
                            final guard =
                                Provider.of<GuardModel>(context, listen: false);
                            guard.clearGuard().then((_) {
                              final services = locator<Auth>();
                              services.logout().then((_) {
                                Navigator.pushReplacementNamed(context, '/');
                              });
                            });
                          })
                    ],
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}
