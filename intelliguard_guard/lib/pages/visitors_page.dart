import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intelliguard_guard/model/ui_color.dart';
import 'package:intelliguard_guard/model/visitor.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:intelliguard_guard/provider_model/visitors_model.dart';
import 'package:intelliguard_guard/ui/label.dart';
import 'package:intelliguard_guard/ui/list_shape.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';

class VisitorsPage extends StatelessWidget {
  final UiColor color = UiColor();

  Widget _buildContent(Visitor visitor) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ListTitle(visitor.name),
              Label(
                label: visitor.present ? 'PRESENT' : 'ABSENT',
                color: visitor.present ? color.green : color.red,
              ),
            ],
          ),
          SizedBox(
            height: 3,
          ),
          Description(visitor.ic),
          Description('${visitor.age} years old')
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final visitors = Provider.of<VisitorsModel>(context);
    final event = Provider.of<CurrentEventModel>(context);
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(child: PageTitle('Visitors')),
            StreamBuilder<QuerySnapshot>(
                stream: visitors.loadVisitors(event.snapshot.documentID),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    int present = 0;
                    int absent = 0;
                    final data = snapshot.data;
                    final docs = data.documents;
                    docs.forEach((v) {
                      final data = v.data;
                      data['present'] ? present++ : absent++;
                    });
                    return Column(
                      children: <Widget>[
                        Text(
                          'Present: $present',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Pier',
                            fontWeight: FontWeight.w700,
                            fontSize: 15,
                          ),
                        ),
                        Text(
                          'Absent: $absent',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Pier',
                            fontWeight: FontWeight.w700,
                            fontSize: 15,
                          ),
                        ),
                      ],
                    );
                  } else {
                    return CircularProgressIndicator(
                      backgroundColor: Colors.white,
                    );
                  }
                })
          ],
        ),
        SizedBox(
          height: 22,
        ),
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
              stream: visitors.loadVisitors(event.snapshot.documentID),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final data = snapshot.data;
                  final docs = data.documents;
                  List<Visitor> visitors = docs.map((doc) {
                    return Visitor(doc['name'], doc.documentID, doc['age'],
                        doc['present']);
                  }).toList();
                  return ListView.builder(
                    itemCount: visitors.length,
                    itemBuilder: (BuildContext context, int index) => ListShape(
                      _buildContent(visitors[index]),
                    ),
                  );
                } else {
                  return Center(
                      child: SizedBox(
                          height: 100,
                          width: 100,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          )));
                }
              }),
        ),
      ],
    );
  }
}
