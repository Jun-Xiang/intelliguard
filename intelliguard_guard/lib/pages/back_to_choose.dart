import 'package:flutter/material.dart';
import 'package:intelliguard_guard/pages/choose_event.dart';
import 'package:intelliguard_guard/provider_model/guard_model.dart';
import 'package:intelliguard_guard/provider_model/problems_model.dart';
import 'package:intelliguard_guard/provider_model/tickets_model.dart';
import 'package:provider/provider.dart';

backToChoose(context) async {
  final guard = Provider.of<GuardModel>(context, listen: false);
  final tickets = Provider.of<TicketsModel>(context, listen: false);
  final problems = Provider.of<ProblemsModel>(context, listen: false);
  final events = await guard.loadCurrentEvent();
  bool isHead = guard.guard.isHeadGuard;
  String page;
  if (isHead)
    page = 'headguard';
  else
    page = 'guard';
  tickets.cancelEvent();
  problems.cancelEvent();
  Navigator.push(
      context, MaterialPageRoute(builder: (_) => ChooseEvent(events, page)));
}
