import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_guard/model/ticket.dart';
import 'package:intelliguard_guard/model/ui_color.dart';
import 'package:intelliguard_guard/pages/guard/show_form.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:intelliguard_guard/provider_model/guard_model.dart';
import 'package:intelliguard_guard/provider_model/tickets_model.dart';
import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/ui/label.dart';
import 'package:intelliguard_guard/ui/list_button.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';

class TicketList extends StatefulWidget {
  final Ticket ticket;

  TicketList(this.ticket);
  @override
  TicketListState createState() => TicketListState();
}

class TicketListState extends State<TicketList> {
  UiColor color = UiColor();
  Color _levelColor;
  Color _statusColor;
  bool _showAcceptButton = false;
  bool _pressed = false;

  @override
  Widget build(BuildContext context) {
    Api _api = locator<Api>();
    final ticket = widget.ticket;
    final tickets = Provider.of<TicketsModel>(context, listen: false);
    final guard = Provider.of<GuardModel>(context, listen: false);
    final event = Provider.of<CurrentEventModel>(context, listen: false);
    switch (ticket.status) {
      case 'SOLVED':
        _statusColor = color.green;
        setState(() {
          _pressed = false;
          _showAcceptButton = false;
        });
        break;
      case 'SOLVING':
        _statusColor = color.orange;
        if (guard.guard.ticketId.indexOf(ticket.id) != -1) {
          setState(() {
            _pressed = true;
          });
        }
        _showAcceptButton = false;

        break;
      case 'UNSOLVED':
        _statusColor = color.red;
        setState(() {
          _showAcceptButton = !_pressed;
        });
        break;
    }
    switch (ticket.level) {
      case 1:
        _levelColor = color.green;
        break;
      case 2:
        _levelColor = color.orange;
        break;
      case 3:
        _levelColor = color.red;
        break;
    }
    double _height = _pressed ? 250 : null;

    void _accepted() {
      setState(() {
        _pressed = true;
      });
    }
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.only(top: 15, right: 20, bottom: 15),
      decoration: BoxDecoration(
        color: color.accentColor,
        borderRadius: BorderRadius.circular(10),
      ),
      height: _height,
      child: Padding(
        padding: EdgeInsets.only(left: 20),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 8,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          ListTitle(ticket.place),
                          Label(
                            label: 'LEVEL ${ticket.level}',
                            color: _levelColor,
                          ),
                          Label(label: ticket.status, color: _statusColor)
                        ],
                      ),
                      Description(ticket.desc),
                      _pressed
                          ? Padding(
                              padding: EdgeInsets.only(top: 100),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Material(
                                    type: MaterialType.transparency,
                                    child: IconButton(
                                      icon: SvgPicture.asset(
                                          'assets/images/icon_done.svg'),
                                      onPressed: () {
                                        tickets.updateStatus(
                                            event.snapshot.documentID,
                                            ticket.id,
                                            'SOLVED');
                                        guard.removeTicketId(ticket.id);
                                      },
                                    ),
                                  ),
                                  Material(
                                    type: MaterialType.transparency,
                                    child: IconButton(
                                      icon: SvgPicture.asset(
                                          'assets/images/icon_report.svg'),
                                      onPressed: () {
                                        showForm(context, ticket);
                                      },
                                    ),
                                  )
                                ],
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
                _showAcceptButton
                    ? ListButton(
                        svg: './assets/images/icon_accept.svg',
                        handlePress: () {
                          _accepted();
                          tickets.updateStatus(
                              event.snapshot.documentID, ticket.id, 'SOLVING');
                          guard.addTicketId(ticket.id);
                        })
                    : Flexible(
                        flex: _pressed ? 0 : 2,
                        child: Container(),
                      )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            _pressed
                ? Container()
                : FutureBuilder(
                    future: _api.getImageURL(ticket.imagePath),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        return Image.network(
                          snapshot.data,
                          loadingBuilder: (context, child,
                              ImageChunkEvent loadingProgress) {
                            if (loadingProgress == null) return child;
                            return CircularProgressIndicator(
                              backgroundColor: Colors.white,
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                  : null,
                            );
                          },
                        );
                      } else {
                        return CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        );
                      }
                    })
          ],
        ),
      ),
    );
  }
}
