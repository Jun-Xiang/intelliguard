import 'package:flutter/material.dart';
import 'package:intelliguard_guard/model/ticket.dart';
import 'package:intelliguard_guard/model/ui_color.dart';
import 'package:intelliguard_guard/pages/guard/report_form.dart';

showForm(BuildContext context, Ticket ticket) {
  final UiColor color = UiColor();
  showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: color.grey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(67),
          topRight: Radius.circular(67),
        ),
      ),
      context: context,
      builder: (BuildContext context) => ReportForm(ticket) );
}
