import 'package:flutter/material.dart';
import 'package:intelliguard_guard/model/ticket.dart';
import 'package:intelliguard_guard/provider_model/guard_model.dart';
import 'package:intelliguard_guard/provider_model/problems_model.dart';
import 'package:intelliguard_guard/ui/form.dart';
import 'package:intelliguard_guard/ui/title.dart';
import 'package:provider/provider.dart';
import 'package:intelliguard_guard/provider_model/current_event_model.dart';
import 'package:intelliguard_guard/pages/camera.dart';

class ReportForm extends StatefulWidget {
  final Ticket ticket;
  ReportForm(this.ticket);
  @override
  _ReportFormState createState() => _ReportFormState();
}

class _ReportFormState extends State<ReportForm> {
  final _formKey = GlobalKey<FormState>();
  int group = 1;
  String desc;
  void _getData(String input) => desc = input;

  void _handlePress(path) {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      final problems = Provider.of<ProblemsModel>(context, listen: false);
      final guard = Provider.of<GuardModel>(context, listen: false);
      final event = Provider.of<CurrentEventModel>(context, listen: false);
      Map<String, dynamic> problem = {
        "place": widget.ticket.place,
        "desc": desc,
        "level": group,
        "reported": false,
        "reported_by": guard.guard.id,
        "image_path": path,
      };
      problems.addProblem(event.snapshot.documentID, problem);
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.only(top: height * 0.05),
      margin: EdgeInsets.symmetric(horizontal: width * 0.075),
      height: height * 0.7 + MediaQuery.of(context).viewInsets.bottom,
      child: Column(
        children: <Widget>[
          FormTitle('Report Form'),
          SizedBox(
            height: height * 0.05,
          ),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: height * 0.02,
                ),
                RadioSelection(
                  group: group,
                  values: [1, 2, 3],
                  title: 'Severity',
                  handleChange: (value) {
                    setState(() {
                      group = value;
                    });
                  },
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                FormInput(
                  placeholder: 'Description',
                  getData: _getData,
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                Camera(_handlePress)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
