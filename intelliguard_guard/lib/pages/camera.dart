import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/ui/alert_error.dart';
import 'package:intelliguard_guard/ui/form.dart';

class Camera extends StatefulWidget {
  final Function handlePress;
  final imagePath;
  Camera(this.handlePress, {this.imagePath});
  @override
  _CameraState createState() => _CameraState();
}

class _CameraState extends State<Camera> {
  Api _api = locator<Api>();
  File _imageFile;
  var _imagePath;
  @override
  void initState() {
    _imagePath = widget.imagePath;
    super.initState();
  }
  FirebaseStorage _storage =
      FirebaseStorage(storageBucket: 'gs://intelliguard-baf01.appspot.com');

  // capture image
  Future<void> _pickImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _imageFile = image;
    });
  }

  //remove image
  void _clear() {
    setState(() {
      _imageFile = null;
      _imagePath = null;
    });
  }

  //upload to storage function
  String _upload() {
    String unique = DateTime.now().toString();
    String filePath = '$unique.png';
    _storage.ref().child(filePath).putFile(_imageFile);
    return filePath;
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        if (_imageFile == null && _imagePath == null)
          FlatButton(
            child: Icon(Icons.photo_camera),
            onPressed: _pickImage,
          ),
        if (_imageFile != null) ...[
          Image.file(_imageFile),
          FlatButton(
            child: Icon(Icons.refresh),
            onPressed: _clear,
          )
        ],
        if (_imageFile == null && _imagePath != null) ...[
           FutureBuilder(
              future: _api.getImageURL(_imagePath),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Image.network(
                    snapshot.data,
                    loadingBuilder:
                        (context, child, ImageChunkEvent loadingProgress) {
                      if (loadingProgress == null) return child;
                      return CircularProgressIndicator(
                        backgroundColor: Colors.white,
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes
                            : null,
                      );
                    },
                  );
                } else {
                  return CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  );
                }
              }),
          FlatButton(
            child: Icon(Icons.refresh),
            onPressed: _clear,
          )
        ],
        SizedBox(
          height: height * 0.06,
        ),
        Button(
          text: 'REPORT',
          handlePress: () {
            if(_imagePath == null && _imageFile == null)
            alertError(context, 'Please send an image for clearer understanding of the problem.');
            else
            if(_imagePath != null)
            widget.handlePress(_imagePath);
            else
            widget.handlePress(_upload());
          },
        )
      ],
    );
  }
}
