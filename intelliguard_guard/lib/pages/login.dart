import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intelliguard_guard/model/ui_color.dart';
import 'package:intelliguard_guard/pages/choose_event.dart';
import 'package:intelliguard_guard/pages/login_guard.dart';
import 'package:intelliguard_guard/services/auth.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/ui/alert_error.dart';
import 'package:intelliguard_guard/ui/form.dart';
import 'package:provider/provider.dart';
import 'package:intelliguard_guard/provider_model/guard_model.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formkey = GlobalKey<FormState>();
  Auth services = locator<Auth>();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String _orgName;
  String _pw;
  UiColor color = UiColor();
  bool obscure = true;
  _showHidePw() {
    setState(() {
      obscure = !obscure;
    });
  }

  void _getOrgName(input) => _orgName = input;
  void _getPw(input) => _pw = input;
  bool _keyboardIsVisible() {
    return !(MediaQuery.of(context).viewInsets.bottom == 0.0);
  }

  void _handlePress() async {
    final form = _formkey.currentState;
    if (form.validate()) {
      form.save();
      FocusScope.of(context).requestFocus(FocusNode());
      bool exists = await services.orgExists(_orgName);
      if (exists) {
        String pw = await services.orgPassword(_orgName);
        if (pw == _pw) {
          services.registerOrg(_orgName);
          showModalBottomSheet(
              isScrollControlled: true,
              backgroundColor: color.grey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(67),
                  topRight: Radius.circular(67),
                ),
              ),
              context: context,
              builder: (_) => LoginGuard(_orgName));
        } else {
          alertError(context, 'Incorrect Password');
        }
      } else {
        alertError(context, 'Organisation doesn\'t exist!');
      }
    }
  }

  Future onSelectNotification(String payload) async {
    switch (payload) {
      case "ticket":
        break;
      case "problem":
        break;
    }
  }

  @override
  void initState() {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    Future showNotification(String type) async {
      var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
          'your channel id', 'your channel name', 'your channel description',
          importance: Importance.Max, priority: Priority.High);
      var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
      var platformChannelSpecifics = new NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
        0,
        'New $type available',
        'Please go to $type page to take action',
        platformChannelSpecifics,
        payload: type,
      );
    }

    setupNotificationLocator(showNotification);
    services.isLoggedIn().then((isLoggedIn) async {
      if (isLoggedIn) {
        String page;
        final guard = Provider.of<GuardModel>(context, listen: false);
        final events = await guard.loadCurrentEvent();
        services.getID().then((id) {
          guard.addGuard(id).then((isHead) {
            if (isHead)
              page = 'headguard';
            else
              page = 'guard';
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (_) => ChooseEvent(events, page)));
          }, onError: (error) {
            alertError(context, error);
          });
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color.fromRGBO(14, 24, 38, 1),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: width * 0.075),
          alignment: Alignment.center,
          child: SingleChildScrollView(
            // shrinkWrap: true,
            physics: _keyboardIsVisible()
                ? BouncingScrollPhysics()
                : NeverScrollableScrollPhysics(),
            child: Column(
              children: <Widget>[
                SvgPicture.asset('assets/images/illustration_login.svg'),
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Text(
                    'INTELLIGUARD',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 34,
                      fontFamily: 'Pier',
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 30),
                  child: Text(
                    'Guard',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 21,
                      fontFamily: 'Pier',
                      fontWeight: FontWeight.w300,
                      color: Colors.white,
                    ),
                  ),
                ),
                Form(
                  key: _formkey,
                  child: Column(
                    children: <Widget>[
                      FormInput(
                        svg: 'assets/images/icon_profile.svg',
                        placeholder: 'Organisation Name',
                        getData: _getOrgName,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      FormInput(
                        svg: 'assets/images/icon_lock.svg',
                        placeholder: 'Password',
                        obscure: obscure,
                        getData: _getPw,
                        showHidePw: _showHidePw,
                      ),
                      SizedBox(
                        height: height * 0.08,
                      ),
                      Button(
                        text: 'LOGIN',
                        handlePress: _handlePress,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
