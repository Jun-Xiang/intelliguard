import 'package:intelliguard_guard/model/organisation.dart';

class OrganisationModel {
  Organisation _org = Organisation(
      'Chung Ling High School', '123', 'worker', 'guard', ['0001', '0002']);

  void loadOrg() {
    //load from firebase;
  }
  get eventsId => List.from(_org.eventsId);
  get workerPw => _org.orgWorkerPassword;
  get guardPw => _org.orgGuardPassword;
  get orgPw => _org.orgPassword;

  set orgPw(pw) => _org = Organisation(_org.orgName, pw, _org.orgWorkerPassword,
      _org.orgGuardPassword, _org.eventsId);
  set workerPw(pw) => _org = Organisation(
      _org.orgName, _org.orgPassword, pw, _org.orgGuardPassword, _org.eventsId);
  set guardPw(pw) => _org = Organisation(_org.orgName, _org.orgPassword,
      _org.orgWorkerPassword, pw, _org.eventsId);
  set addEvent(id) => _org.eventsId.add(id);
}
