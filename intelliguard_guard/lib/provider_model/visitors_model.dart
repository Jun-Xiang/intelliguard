import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intelliguard_guard/model/visitor.dart';
import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/locator.dart';

class VisitorsModel {
  List<Visitor> _visitors = [];
  Api _api = locator<Api>();
  
  get allVisitors => List.from(_visitors);
  void addVisitor(Visitor visitor) => _visitors.add(visitor);
  Stream<QuerySnapshot> loadVisitors(String id){
    return _api.getVisitors(id);
  }
}