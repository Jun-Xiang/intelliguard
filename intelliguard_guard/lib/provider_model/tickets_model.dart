import 'dart:async';

import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/services/show_notifications.dart';

class TicketsModel {
  Api _api = locator<Api>();
  List tickets = [];
  StreamSubscription<dynamic> _ticketEvent;
  ShowNotification _showNotification = locator<ShowNotification>();

  void addTicket(eventID, Map<String, dynamic> ticket) {
    _api.addTicket(eventID, ticket);
  }

  void removeTicket(String eventID, String ticketID) {
    _api.deleteTicket(eventID, ticketID);
  }

  

  loadTicket(String id) {
    return _api.getTickets(id);
  }

  void startEvent(String id, bool isHead) {
     _ticketEvent = _api.getTickets(id).listen((snapshot) {
      if (snapshot.documentChanges.length != tickets.length) {
        tickets = snapshot.documentChanges;
          if(!isHead){
            //send notifications
            _showNotification.showNotification('ticket');
          }
      }
    });
  }

  updateStatus(String eventID, String id, String status) {
    _api.updateTicketStatus(eventID, id, status);
  }
  cancelEvent(){
    _ticketEvent.cancel();
    tickets = [];
  }
}
