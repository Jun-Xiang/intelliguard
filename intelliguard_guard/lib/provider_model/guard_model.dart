import 'dart:async';

import 'package:intelliguard_guard/model/guard.dart';
import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class GuardModel {
  Guard _guard;
  Api _api = locator<Api>();
  StreamSubscription<dynamic> _ticketIDAddEvent;
  StreamSubscription<dynamic> _ticketIDRemoveEvent;

  Future<bool> addGuard(id) async {
    final snapshot = await _api.getOrgDetails();
    final val = await snapshot.value;
    final guardId = [];
    val['guard'].forEach((i, x) {
      guardId.add(i);
    });
    if (!guardId.contains(id)) throw ('No ID found!');
    bool isHead = val['guard'][id]['isHead'];
    _guard = Guard(id, [], isHead);
    _ticketIDAddEvent = _api.ref.child('guard/$id').onChildAdded.listen((data) {
      if (data.snapshot.value.runtimeType != bool)
      Map.from(data.snapshot.value).keys.toList().forEach((id){
        _guard.ticketId.add(id);
      });
    });
    _ticketIDRemoveEvent =
        _api.ref.child('guard/$id').onChildRemoved.listen((data) {
      _guard.ticketId.removeWhere(
          (id) => id == Map.from(data.snapshot.value).keys.toList()[0]);
    });
    return isHead;
  }

  Future<List<DocumentSnapshot>> loadCurrentEvent() async {
    final snapshot = await _api.getDataCollection();
    final happeningEvents = snapshot.documents.where((doc) {
      final dateAry = doc.data['date'].split('-');
      final startYr = int.parse(dateAry[0]);
      final startMn = int.parse(dateAry[1]);
      final startDy = int.parse(dateAry[2]);
      final endYr = int.parse(dateAry[3]);
      final endMn = int.parse(dateAry[4]);
      final endDy = int.parse(dateAry[5]);
      final start = DateTime(startYr, startMn, startDy);
      final end = DateTime(endYr, endMn, endDy);
      final current = DateTime.now();
      bool isHappening = start.difference(current).inDays >= 0 ||
          end.difference(current).inDays >= 0;
      return isHappening;
    });
    return happeningEvents.toList();
  }

  void addTicketId(id) {
    _api.addTicketIDToGuard(_guard.id, id);
  }

  void removeTicketId(id) {
    _api.removeTicketIDFromGuard(_guard.id, id);
  }

  get guard => _guard;
  Future<Null> clearGuard() async {
    await _ticketIDAddEvent.cancel();
    await _ticketIDRemoveEvent.cancel();
    resetOrgLocator();
    _guard = null;
  }
}
