import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intelliguard_guard/services/api.dart';
import 'package:intelliguard_guard/services/locator.dart';
import 'package:intelliguard_guard/services/show_notifications.dart';

class ProblemsModel {
  Api _api = locator<Api>();
  List problems = [];
  StreamSubscription<dynamic> _problemEvent;
  ShowNotification _showNotification = locator<ShowNotification>();

  void addProblem(eventID, Map<String, dynamic> problem) {
    _api.addProblem(eventID, problem);
  }

  Stream<QuerySnapshot> loadProblem(eventID) {
    return _api.getProblems(eventID);
  }

  void startEvent(eventID , bool isHead) {
    _problemEvent = _api.getProblems(eventID).listen((snapshot) {
      if (snapshot.documentChanges.length != problems.length) {
        problems = snapshot.documentChanges;
        if (isHead) {
          //send notifications
          //'new problem'
          _showNotification.showNotification('problem');
        }
      }
    });
  }

  void reported(String eventID, String id) {
    _api.updateProblemStatus(eventID, id, true);
  }

  void cancelEvent() {
    _problemEvent.cancel();
    problems = [];
  }
}
