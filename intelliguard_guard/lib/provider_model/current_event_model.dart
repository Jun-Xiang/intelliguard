import 'package:cloud_firestore/cloud_firestore.dart';

class CurrentEventModel {
  DocumentSnapshot snapshot;

  set setEvent(event) => snapshot = event;

  reset() => snapshot = null;
}