const express = require('express');
const app = express();
const helmet = require('helmet');
const cors = require('cors');
const JsBarcode = require('jsbarcode');
const { createCanvas } = require('canvas');
const nodemailer = require('nodemailer');

require('dotenv').config();
const PORT = process.env.PORT || 8000;
app.use(helmet());
app.use(cors());
app.use(express.json());

app.get('/', (req,res)=>{
    res.send('hello');
})

app.get('/hello', (req,res)=>{
    res.send('hello from hello');
})

app.post('/sendEmail', (req, res)=>{
    const {ic, email, date, time, name, venue, event_id, org,} = req.body;
    const canvas = createCanvas();
    JsBarcode(canvas, `events_${org},${event_id},${ic}`, {
        text: name
    });
    const transport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        secure: true,
        port: 465,
        auth: {
            user: process.env.EMAIL,
            pass: process.env.PASSWORD
        }
    });
    const html = `<h1>Event details</h1><br/><p>Event: ${name}<br/>Date: ${date}<br/>Time: ${time}<br/>Venue: ${venue}</p>`;
    const mailOptions = {
        from: process.env.EMAIL,
        to: email,
        subject: `${name} Event Barcode`,
        html,
        attachments: [{
            filename: 'barcode.png',
            content: canvas.toDataURL().split('base64,')[1],
            encoding: 'base64'
        }]

    }
    transport.sendMail(mailOptions, (error, info)=>{
        if(error){
            res.send(error);
        }else{
            res.send(info.response);
        }
    })
})

app.listen(PORT);